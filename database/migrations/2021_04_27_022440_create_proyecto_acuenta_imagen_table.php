<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProyectoAcuentaImagenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyecto_acuenta_imagenes', function (Blueprint $table) {
            $table->id();
            $table->string('url');
            $table->timestamps();
        });
        Schema::table('proyecto_acuenta_imagenes', function (Blueprint $table) {
            $table->bigInteger('proyecto_pagos_cuenta_id')->unsigned();
            $table->foreign('proyecto_pagos_cuenta_id')->references('id')->on('proyecto_pagos_cuenta');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyecto_acuenta_imagenes');
    }
}
