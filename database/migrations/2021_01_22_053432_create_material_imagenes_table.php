<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaterialImagenesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_imagenes', function (Blueprint $table) {
            $table->id();
            $table->string('url');
            $table->timestamps();
        });
        Schema::table('material_imagenes', function (Blueprint $table) {
            $table->bigInteger('material_id')->unsigned();
            $table->foreign('material_id')->references('id')->on('pp_materiales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_imagenes');
    }
}
