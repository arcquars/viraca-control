<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('ci')->nullable(true);
            $table->string('nombres', 200);
            $table->string('apellido_paterno', 200);
            $table->string('apellido_materno', 200)->nullable(true);
            $table->string('telefono', 15)->nullable(true);
            $table->string('direccion')->nullable(true);

            $table->boolean('deleted')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}
