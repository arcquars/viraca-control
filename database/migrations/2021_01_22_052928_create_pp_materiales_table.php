<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePpMaterialesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pp_materiales', function (Blueprint $table) {
            $table->id();

            $table->bigInteger('persona_id');
            $table->bigInteger('proyecto_id');

            $table->float('cantidad', 8, 2);
            $table->date('fecha');
            $table->string('unidad');
            $table->text('descripcion')->nullable();

            $table->bigInteger('user_id')->nullable(true);
            $table->boolean('deleted')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pp_materiales');
    }
}
