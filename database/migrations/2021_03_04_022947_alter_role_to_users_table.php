<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use \Illuminate\Support\Facades\DB;

class AlterRoleToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
//            $table->enum('role', ['ADMIN', 'MANAGER_PROJECT', 'SUPERVISOR', 'BUILDER'])->default('ADMIN')->change();
            DB::statement("ALTER TABLE users CHANGE role role ENUM('ADMIN', 'MANAGER_PROJECT', 'SUPERVISOR', 'BUILDER') DEFAULT 'ADMIN'");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
//            $table->enum('role', ['ADMIN', 'MANAGER_PROJECT'])->default('ADMIN');
            DB::statement("ALTER TABLE users CHANGE role role ENUM('ADMIN', 'MANAGER_PROJECT') DEFAULT 'ADMIN'");
        });
    }
}
