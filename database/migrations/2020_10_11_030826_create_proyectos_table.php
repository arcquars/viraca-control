<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProyectosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyectos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre')->nullable(false);
            $table->text('descripcion')->nullable(true);
            $table->double('presupuesto',10, 2)->default(0);
            $table->double('gastos',10, 2)->default(0);
            $table->string('estado')->nullable(true);
            $table->date('fecha_inicio')->nullable(true);
            $table->date('fecha_fin')->nullable(true);
            $table->boolean('deleted')->default(false);

            $table->timestamps();
        });
        Schema::table('proyectos', function (Blueprint $table) {
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyectos');
    }
}
