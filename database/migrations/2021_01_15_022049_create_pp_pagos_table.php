<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePpPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pp_pagos', function (Blueprint $table) {
            $table->id();

            $table->bigInteger('persona_id');
            $table->bigInteger('proyecto_id');

            $table->float('monto', 8, 2);
            $table->date('fecha');
            $table->text('descripcion')->nullable();
            $table->boolean('deleted')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pp_pagos');
    }
}
