<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProyectoPersonaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyecto_persona', function (Blueprint $table) {
            $table->bigInteger('persona_id')->unsigned();
            $table->bigInteger('proyecto_id')->unsigned();
            $table->foreign('persona_id')->references('id')->on('personas')
                ->onDelete('cascade');
            $table->foreign('proyecto_id')->references('id')->on('proyectos')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyecto_persona');
    }
}
