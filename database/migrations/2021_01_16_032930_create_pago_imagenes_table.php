<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagoImagenesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pago_imagenes', function (Blueprint $table) {
            $table->id();
            $table->string('url');
            $table->timestamps();
        });
        Schema::table('pago_imagenes', function (Blueprint $table) {
            $table->bigInteger('pago_id')->unsigned();
            $table->foreign('pago_id')->references('id')->on('pp_pagos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pago_imagenes');
    }
}
