<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagoCuentaProyectoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyecto_pagos_cuenta', function (Blueprint $table) {
            $table->id();

            $table->float('monto', 8, 2);
            $table->date('fecha');
            $table->text('descripcion')->nullable();
            $table->boolean('deleted')->default(false);

            $table->timestamps();
        });

        Schema::table('proyecto_pagos_cuenta', function (Blueprint $table) {
            $table->unsignedBigInteger('proyecto_id');
            $table->foreign('proyecto_id')->references('id')->on('proyectos');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyecto_pagos_cuenta');
    }
}
