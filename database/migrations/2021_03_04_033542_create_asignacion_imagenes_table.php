<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAsignacionImagenesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asignacion_imagenes', function (Blueprint $table) {
            $table->id();

            $table->string('url');

            $table->timestamps();
        });

        Schema::table('asignacion_imagenes', function (Blueprint $table) {
            $table->bigInteger('asignacion_id')->unsigned();
            $table->foreign('asignacion_id')->references('id')->on('pm_asignaciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asignacion_imagenes');
    }
}
