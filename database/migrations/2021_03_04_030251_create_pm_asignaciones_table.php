<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePmAsignacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pm_asignaciones', function (Blueprint $table) {
            $table->id();

            $table->bigInteger('proyecto_id');
            $table->bigInteger('manager_id');

            $table->bigInteger('user_id');

            $table->float('monto', 8, 2);
            $table->date('fecha');
            $table->text('descripcion')->nullable();
            $table->boolean('deleted')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pm_asignaciones');
    }
}
