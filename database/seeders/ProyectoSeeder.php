<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProyectoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('proyectos')->insert([
            'nombre' => 'Proyecto 1',
            'descripcion' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy).',
            'presupuesto' => 5800,
            'gastos' => 0,
            'estado' => 'NUEVO',
            'fecha_inicio' => Carbon::parse('2020-10-11'),
            'fecha_fin' => Carbon::parse('2020-11-30'),
            'deleted' => false,
            'user_id' => 3
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'Proyecto 2',
            'descripcion' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy).',
            'presupuesto' => 6800,
            'gastos' => 0,
            'estado' => 'NUEVO',
            'fecha_inicio' => Carbon::parse('2020-10-15'),
            'fecha_fin' => Carbon::parse('2020-12-20'),
            'deleted' => false,
            'user_id' => 3
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'Proyecto 3',
            'descripcion' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy).',
            'presupuesto' => 7800,
            'gastos' => 0,
            'estado' => 'NUEVO',
            'fecha_inicio' => Carbon::parse('2020-10-21'),
            'fecha_fin' => Carbon::parse('2020-11-30'),
            'deleted' => false,
            'user_id' => 3
        ]);
    }
}
