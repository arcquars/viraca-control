<?php
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\Admin\HomeController;
use \App\Http\Controllers\Admin\ProyectoController;
use \App\Http\Controllers\Admin\UserController;

Route::get('', [HomeController::class, 'index']);
//Route::get('/proyectos', [ProyectoController::class, 'index']);
Route::get('/proyectos/list', [ProyectoController::class, 'getProyectos'])->name('proyectos.list');
Route::resource('/proyectos', ProyectoController::class);
// Usuarios
Route::get('/users/list', [UserController::class, 'getUsers'])->name('users.list');
Route::resource('/users', UserController::class);