<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\Api\PersonaController;

use \App\Http\Controllers\Api\ProyectoController;
use \App\Http\Controllers\Api\PagoController;
use \App\Http\Controllers\Api\MaterialController;
use \App\Http\Controllers\Api\UserController;
use \App\Http\Controllers\Api\AsignacionController;
use \App\Http\Controllers\Api\ProyectoPagoAcuentaController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('get-lock-username', '\App\Http\Controllers\Auth\AuthController@getLockByUsername');
    Route::post('login', '\App\Http\Controllers\Auth\AuthController@login');
    Route::post('signup', '\App\Http\Controllers\Auth\AuthController@signUp');

    Route::group([
        'middleware' => 'auth:sanctum'
    ], function() {
        Route::get('logout', '\App\Http\Controllers\Auth\AuthController@logout');
        Route::get('user', '\App\Http\Controllers\Auth\AuthController@user');

        // User Api
        Route::post('users/update-lock/{userId}', [UserController::class, 'updateLock']);
        Route::resource('users', UserController::class);

        // Proyectos
        Route::post('proyectos/get-manager-no-proyecto/{proyectoId}', [ProyectoController::class, 'getManagerByNotProyectoSearch']);
        Route::post('proyectos/get-proyectos-manager', [ProyectoController::class, 'getProyectosByManager']);

        Route::get('proyectos/get-total-gastos/{proyectoId}', [ProyectoController::class, 'getTotalGastos'])->name('proyectos.get.total.gastos');
        Route::get('proyectos/get-manager-total-gastos/{proyectoId}/{managerId}', [ProyectoController::class, 'getTotalManagerGastos'])->name('proyectos.manager.get.total.gastos');
        Route::post('proyectos/asignar-proyecto-usuario', [ProyectoController::class, 'assignUserProyecto'])->name('proyectos.asignar.proyecto.usuario');
        Route::post('proyectos/desasignar-proyecto-usuario', [ProyectoController::class, 'unassignUserProyecto'])->name('proyectos.desasignar.proyecto.usuario');
        Route::post('proyectos/asignar-proyecto-persona', [ProyectoController::class, 'assignPersonaProyecto'])->name('proyectos.asignar.proyecto.persona');
        Route::post('proyectos/asignar-proyecto-manager', [ProyectoController::class, 'assignManagerProyecto'])->name('proyectos.asignar.proyecto.manager');
        Route::get('proyectos/get-managers/{proyectoId}', [ProyectoController::class, 'getManagersByProyectoId'])->name('proyectos.managers');
        Route::get('proyectos/get-personas/{proyectoId}', [ProyectoController::class, 'getPersonasByProyectoId'])->name('proyectos.personas');
        Route::get('proyectos/get-personas/{proyectoId}/{managerId}', [ProyectoController::class, 'getPersonasByProyectoIdAndManagerId'])->name('proyectos.manager.personas');
        Route::post('proyectos/get-managers-no-proyecto/{proyectoId}', [ProyectoController::class, 'getManagersByNotProyectoSearch']);
        Route::resource('proyectos', ProyectoController::class);

        // Personas
        Route::get('personas/get-personas-no-proyecto/{proyectoId}', [PersonaController::class, 'getPersonByNotProyecto']);
        Route::post('personas/get-personas-no-proyecto/{proyectoId}', [PersonaController::class, 'getPersonByNotProyectoSearch']);
        Route::resource('personas', PersonaController::class);

        //Pagos
        Route::get('pagos/get-pagos-imagen-by-pago-id/{pagoId}', [PagoController::class, 'getPagoImagesByPagoId']);
        Route::get('pagos/get-pagos-by-person/{proyectoId}/{personId}', [PagoController::class, 'getPagosByProyectoPerson']);
        Route::post('pagos/get-pagos-by-person-fecha', [PagoController::class, 'getPagosByProyectoPersonBeetweenDate']);
        Route::post('pagos/get-pagos-by-manager-fecha', [PagoController::class, 'getPagosByProyectoManagerBeetweenDate']);
        Route::resource('pagos', PagoController::class);

        // Materiales
        Route::get('materiales/get-materiales-imagen-by-material-id/{materialId}', [MaterialController::class, 'getMaterialImagesByMaterialId']);
        Route::post('materiales/get-materials-by-person-fecha', [MaterialController::class, 'getMaterialByProyectoPersonBeetweenDate']);
        Route::post('materiales/get-materials-by-manager-fecha', [MaterialController::class, 'getMaterialByProyectoManagerBeetweenDate']);
        Route::resource('materiales', MaterialController::class);

        // Asignacion dinero a usuario por proyecto
        Route::get('asignacion/get-images-asignacion/{asignacionId}', [AsignacionController::class, 'getAsignacionImagesById']);
        Route::get('asignacion/get-all-managers', [AsignacionController::class, 'getAllManagers']);
        Route::post('asignacion/get-total-proyecto', [AsignacionController::class, 'getTotalAsignateByProyect']);
        Route::post('asignacion/get-total-proyecto-manager', [AsignacionController::class, 'getTotalAsignateByProyectAndManager']);
        Route::post('asignacion/search-proyecto-manager-fechas', [AsignacionController::class, 'getPagosByProyectoManagerBeetweenDate']);
        Route::resource('asignacion', AsignacionController::class);

        // Proyecto pagos a cuenta
        Route::post('proyecto-pago-acuenta/search-proyecto-acuenta-fechas', [ProyectoPagoAcuentaController::class, 'getAcuentaByProyectoManagerBeetweenDate']);
        Route::get('proyecto-pago-acuenta/get-images-acuenta/{proyectoPagoId}', [ProyectoPagoAcuentaController::class, 'getAcuentaImagesById']);
        Route::post('proyecto-pago-acuenta/get-total-acuenta-proyecto', [ProyectoPagoAcuentaController::class, 'getTotalAcuentaProyecto']);
        Route::resource('proyecto-pago-acuenta', ProyectoPagoAcuentaController::class);

    });
});
