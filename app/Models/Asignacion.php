<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Asignacion extends Model
{
    use HasFactory;

    protected $table = "pm_asignaciones";

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'proyecto_id',
        'manager_id',
        'user_id',
        'monto',
        'fecha',
        'descripcion'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'fecha'
    ];

    protected $casts = [
        'fecha'  => 'date:Y-m-d'
    ];

    public static function totalAsignateByProyectAndManager($proyectoId, $managerId){
        return Asignacion::where('deleted', "=", 0)->where('proyecto_id', "=", $proyectoId)->where("manager_id", "=", $managerId)->sum('monto');
    }

    public static function totalAsignateByProyect($proyectoId){
        return Asignacion::where('deleted', "=", 0)->where('proyecto_id', "=", $proyectoId)->sum('monto');
    }

    public static function searchAsignacionByProyectoAndManagerAndFechas($proyectoId, $managerId, $fechaIni, $fechaFin){
        return Asignacion::where('manager_id', '=', $managerId)
            ->where('deleted', '=', 0)
            ->where('proyecto_id', '=', $proyectoId)
            ->whereBetween('fecha', [$fechaIni, $fechaFin])->orderBy('fecha', 'ASC')->get();
    }
}
