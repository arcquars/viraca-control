<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    use HasFactory;

    public function proyectos(){
        return $this->belongsToMany(Proyecto::class, 'proyecto_persona', 'persona_id', 'proyecto_id');
    }
}
