<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    use HasFactory;

    protected $table = "pp_materiales";

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'persona_id',
        'proyecto_id',
        'cantidad',
        'unidad',
        'fecha',
        'descripcion',
        'options->enabled'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'fecha'
    ];

    protected $casts = [
        'fecha'  => 'date:Y-m-d'
    ];
}
