<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProyectoPagoCuenta extends Model
{
    use HasFactory;

    protected $table = "proyecto_pagos_cuenta";

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'proyecto_id',
        'monto',
        'fecha',
        'descripcion',
        'user_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'fecha'
    ];

    protected $casts = [
        'fecha'  => 'date:Y-m-d'
    ];

    public static function getTotalAcuentaProyecto($proyectoId){
        return ProyectoPagoCuenta::where('deleted', '=', 0)->where('proyecto_id', '=', $proyectoId)->sum('monto');
    }

    public static function searchAcuentaByProyectoAndFechas($proyectoId, $fechaIni, $fechaFin){
        return ProyectoPagoCuenta::where('deleted', '=', 0)
            ->where('proyecto_id', '=', $proyectoId)
            ->whereBetween('fecha', [$fechaIni, $fechaFin])->orderBy('fecha', 'ASC')->get();
    }
}
