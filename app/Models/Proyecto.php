<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
    use HasFactory;

    const ESTADO_NUEVO = 'NUEVO';
    const ESTADO_EJECUCION = 'EJECUCION';
    const ESTADO_CERRADO = 'CERRADO';

    protected $table = 'proyectos';

    protected $fillable = [
        'nombre',
        'descripcion',
        'presupuesto',
        'gastos',
        'estado',
        'fecha_inicio',
        'fecha_fin',
        'deleted'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'fecha_inicio',
        'fecha_fin',
    ];

    protected $casts = [
        'fecha_inicio'  => 'date:Y-m-d',
        'fecha_fin' => 'date:Y-m-d',
    ];

    public function users(){
        return $this->belongsToMany(User::class, 'project_managers', 'proyecto_id', 'user_id');
    }

    public function personas(){
        return $this->belongsToMany(Persona::class, 'proyecto_persona', 'proyecto_id', 'persona_id');
    }
}
