<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProyectoAcuentaImagen extends Model
{
    use HasFactory;

    protected $table = "proyecto_acuenta_imagenes";
}
