<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        $id = $this->route('user');

        $email = 'required|email|unique:users,email|max:255';
        $pass = 'required|min:6|nullable|confirmed';
        $pass_c = 'required|min:6|nullable';
//        if(isset($id)){
//            $email = 'required|email|unique:users,email,'.$id.'|max:255';
//            $pass = 'min:6|nullable|confirmed';
//            $pass_c = 'min:6|nullable';
//        }

        return [
            'name' => 'required|max:255',
            'email' => $email,
            'password' => $pass,
            'password_confirmation' => $pass_c,
            'role' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'El nombre es requerido',
            'name.max' => 'El nombre tiene que tener un maximo de 255 caracteres',
            'email.required' => 'El correo es requerido',
            'email.email' => 'El formato del correo electrónico es incorrecto',
            'email.unique' => 'El correo ya esta registrado',
            'email.max' => 'El correo tiene que tener un maximo de 255 caracteres',
            'password.required' => 'La contraseña es requerida',
            'password.min' => 'La contraseña tiene que tener mínimo 6 caracteres',
            'password.confirmed' => 'No coinciden',
            'password_confirmation.required' => 'Campo requerido',
            'password_confirmation' => 'Tiene que tener mínimo 6 caracteres',
            'role.required' => 'El Rol es requerido',
        ];
    }
}
