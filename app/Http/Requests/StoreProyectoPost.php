<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProyectoPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required',
            'presupuesto' => ['required', 'regex:/^\d*(\.\d{1,2})?$/'],
            'fecha_inicio' => 'required|date',
            'fecha_fin' => 'required|date|after:fecha_inicio',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nombre.required' => 'El nombre es requerido',
            'presupuesto.required' => 'El presupuesto es requerido',
            'presupuesto.regex' => 'El presupuesto tiene que tener un valor numerico en formato 100.00',
            'fecha_inicio.required' => 'Fecha es requerido',
            'fecha_inicio.date' => 'Formato de fecha es incorrecto',
            'fecha_fin.required' => 'Fecha es requerido',
            'fecha_fin.date' => 'Formato de fecha es incorrecto',
            'fecha_fin.after' => 'La fecha de fin tiene que ser mayor a la fecha de inicio',
            '' => '',
        ];
    }
}
