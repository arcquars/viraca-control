<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreMaterialPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "persona_id" => "required",
            "proyecto_id" => "required",
            "cantidad" => ["required", 'regex:/^\d*(\.\d{1,2})?$/'],
            "unidad" => "required|string|min:2|max:25",
            "fecha" => "required|date",
            "descripcion" => "nullable|string|min:2|max:600"
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'persona_id.required' => 'Persona es requerido',
            'proyecto_id.required' => 'Proyecto es requerido',
            'cantidad.required' => 'Monto es requerido',
            'cantidad.reqex' => 'Monto no esta en formato (Ej: 100.55)',
            'unidad.required' => 'Unidad es requerido',
            'unidad.min' => 'Unidad tiene que tener por lo menos 2 caracteres',
            'unidad.max' => 'Unidad tiene que tener maxímo 25 caracteres',
            'fecha.required' => 'Fecha es requerido',
            'fecha.date' => 'Formato de fecha es incorrecto',
            'descripcion.min' => 'Descripción tiene que tener minimo 2 caracteres',
            'descripcion.max' => 'Descripción tiene que tener máximo 600 caracteres'
        ];
    }
}
