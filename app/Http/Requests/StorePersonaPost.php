<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePersonaPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ci' => 'nullable|numeric|digits_between:5,12|unique:personas,ci',
            'nombres' => 'required|string|min:3|max:200',
            'apellido_paterno' => 'required|string|min:3|max:200',
            'apellido_materno' => 'string|min:3|max:200|nullable',
            'telefono' => 'nullable|digits_between:7,14',
            'direccion' => 'string|min:5|max:255|nullable',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'ci.numeric' => 'El CI tiene que tener un valor numerico',
            'ci.digits_between' => 'El CI tiene que tener entre 5 y 12 dígitos',
            'ci.unique' => 'CI ya se encuentre registrado',
            'nombres.required' => 'Nombres es requerido',
            'nombres.min' => 'Nombres tiene que tener un mínimo de 3 caracteres',
            'nombres.max' => 'Nombres tiene que tener un máximo de 200 caracteres',
            'apellido_paterno.required' => 'Apellido paterno es requerido',
            'apellido_paterno.min' => 'Apellido paterno tiene que tener un mínimo de 3 caracteres',
            'apellido_paterno.max' => 'Apellido paterno tiene que tener un máximo de 200 caracteres',
            'apellido_materno.min' => 'Apellido materno tiene que tener un mínimo de 3 caracteres',
            'apellido_materno.max' => 'Apellido materno tiene que tener un máximo de 200 caracteres',
            'direccion.min' => 'Dirección tiene que tener un mínimo de 5 caracteres',
            'direccion.max' => 'Dirección tiene que tener un máximo de 255 caracteres',
            'telefono.digits_between' => 'Teléfono tiene que tener un valor numérico y tener 7 y 14 dígitos.',
//            '' => '',
        ];
    }
}
