<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAsignacionPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "proyecto_id" => "required",
            "manager_id" => "required",

            "monto" => ["required"],
//            "monto" => ["required", 'regex:/^\d*(\.\d{1,2})?$/'],
            "fecha" => "required|date",
            "descripcion" => "nullable|string|min:2|max:600"
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'manager_id.required' => 'Usuario es requerido',
            'proyecto_id.required' => 'Proyecto es requerido',
            'monto.required' => 'Monto es requerido',
            'monto.reqex' => 'Monto no esta en formato (Ej: 100.00)',
            'fecha.required' => 'Fecha es requerido',
            'fecha.date' => 'Formato de fecha es incorrecto',
            'descripcion.min' => 'Descripción tiene que tener minimo 2 caracteres',
            'descripcion.max' => 'Descripción tiene que tener máximo 600 caracteres'
        ];
    }
}
