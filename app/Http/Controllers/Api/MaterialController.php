<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreMaterialPost;
use App\Models\Material;
use App\Models\MaterialImagen;
use App\Models\Pago;
use App\Models\PagoImagen;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class MaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMaterialPost $request)
    {
        $material = new Material();
        $material->persona_id = $request->get('persona_id');
        $material->proyecto_id = $request->get('proyecto_id');
        $material->cantidad = $request->get('cantidad');
        $material->unidad = $request->get('unidad');
        $material->fecha = $request->get('fecha');
        $material->descripcion = $request->get('descripcion');
        $material->user_id = Auth::id();
        $material->save();

        $photos = $request->get('photos');

        if($photos){
            foreach ($photos as $index => $photo){
                $data = $photo;
                $datas = explode(",", $data);
                $dataImg = base64_decode($datas[1]);

                $img = imagecreatefromstring($dataImg);
                $fileName = '/img/uploads/material-imagenes/'.$material->id.'-'.$index.'-'.date('YmdHis').'.jpeg';
                imagejpeg($img, public_path().$fileName);

                $materialImagen = new MaterialImagen();
                $materialImagen->url = $fileName;
                $materialImagen->material_id = $material->id;
                $materialImagen->save();
            }
        }
        return response()->json(['material_id' => $material->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $materialImagenes = MaterialImagen::where('material_id', '=', $id)->get();
        foreach ($materialImagenes as $materialI){
            unlink(base_path().'/public'.$materialI->url);
        }
        MaterialImagen::where('material_id', '=', $id)->delete();

        return response()->json(Material::destroy($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function getMaterialByProyectoPersonBeetweenDate(Request $request){
        $proyectoId = $request->post('proyecto_id');
        $personaId = $request->post('persona_id');
        $fechaIni = $request->post('fecha_inicio');
        $fechaFin = $request->post('fecha_fin');
//        return response()->json(['result' => $proyectoId ]);
        return response()->json(
            Material::where('persona_id', '=', $personaId)
                ->where('proyecto_id', '=', $proyectoId)
                ->whereBetween('fecha', [$fechaIni, $fechaFin])->orderBy('fecha', 'ASC')->get());
    }

    public function getMaterialImagesByMaterialId($materialId){
        return response()->json(
            MaterialImagen::where('material_id', '=', $materialId)->get());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function getMaterialByProyectoManagerBeetweenDate(Request $request){
        $proyectoId = $request->post('proyecto_id');
        $userId = $request->post('manager_id');
        $fechaIni = $request->post('fecha_inicio');
        $fechaFin = $request->post('fecha_fin');
//        return response()->json(['result' => $proyectoId ]);
        return response()->json(
            Material::where('user_id', '=', $userId)
                ->where('proyecto_id', '=', $proyectoId)
                ->whereBetween('fecha', [$fechaIni, $fechaFin])->orderBy('fecha', 'ASC')->get());
    }
}
