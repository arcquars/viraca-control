<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Asignacion;
use App\Models\PagoImagen;
use App\Models\ProyectoAcuentaImagen;
use App\Models\ProyectoPagoCuenta;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class ProyectoPagoAcuentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $acuenta = new ProyectoPagoCuenta();

        $acuenta->proyecto_id = $request->get('proyecto_id');
        $acuenta->monto = $request->get('monto');
        $acuenta->fecha = $request->get('fecha');
        $acuenta->descripcion = $request->get('descripcion');
        $acuenta->user_id = Auth::id();
        $acuenta->save();

        $photos = $request->get('photos');

        if($photos){
            foreach ($photos as $index => $photo){
                $data = $photo;
                $datas = explode(",", $data);
                $dataImg = base64_decode($datas[1]);

                $img = imagecreatefromstring($dataImg);
                $fileName = '/img/uploads/acuenta-imagenes/'.$acuenta->id.'-'.$index.'-'.date('YmdHis').'.jpeg';
                imagejpeg($img, public_path().$fileName);

                $paImagen = new ProyectoAcuentaImagen();
                $paImagen->url = $fileName;
                $paImagen->proyecto_pagos_cuenta_id = $acuenta->id;
                $paImagen->save();
            }
        }

        return response()->json(['proyecto_acuenta_id' => $acuenta->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//        $proyectoAcuentaImagen = ProyectoAcuentaImagen::where('proyecto_pagos_cuenta_id', '=', $id)->get();
//        foreach ($proyectoAcuentaImagen as $acuentaI){
//            unlink(base_path().'/public'.$acuentaI->url);
//        }
//        ProyectoAcuentaImagen::where('proyecto_pagos_cuenta_id', '=', $id)->delete();
//
//        return response()->json(ProyectoPagoCuenta::destroy($id));
    }

    /**
     * @param  Request $request
     * @return Response
     */
    public function getTotalAcuentaProyecto(Request $request){
        $proyectoId = $request->post('proyecto_id');
        $totalAcuentaProyecto = ProyectoPagoCuenta::getTotalAcuentaProyecto($proyectoId);
        return response()->json(array('totalAcuentaProyecto' => $totalAcuentaProyecto));
    }

    /**
     * @param  Request $request
     * @return Response
     */
    public function getTotalAcuentaProyectoManager(Request $request){
        $proyectoId = $request->post('proyecto_id');
        $managerId = $request->post('manager_id');
        $totalAcuentaProyectoManager = ProyectoPagoCuenta::getTotalAcuentaProyectoManager($proyectoId, $managerId);
        return response()->json(array('totalAcuentaProyectoManager' => $totalAcuentaProyectoManager));
    }

    public function getAcuentaImagesById($proyectoPagoId){
        return response()->json(
            ProyectoAcuentaImagen::where('proyecto_pagos_cuenta_id', '=', $proyectoPagoId)->get());
    }

    /**
     * @param  Request $request
     * @return Response
     */
    public function getAcuentaByProyectoManagerBeetweenDate(Request $request){
        $proyectoId = $request->post('proyecto_id');
        $fechaIni = $request->post('fecha_inicio');
        $fechaFin = $request->post('fecha_fin');
//        return response()->json(['result' => $proyectoId ]);
        return response()->json(ProyectoPagoCuenta::searchAcuentaByProyectoAndFechas($proyectoId, $fechaIni, $fechaFin));
    }
}
