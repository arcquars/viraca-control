<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProyectoPost;
use App\Models\Asignacion;
use App\Models\Pago;
use App\Models\Persona;
use App\Models\Proyecto;
use App\Models\ProyectoPagoCuenta;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class ProyectoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proyectos = Proyecto::where('deleted', '=', 0)->get();

        $results = array();
        foreach ($proyectos as $proyecto){
            array_push($results, [
                "id" => $proyecto->id,
                "nombre" => $proyecto->nombre,
                "descripcion" => $proyecto->descripcion,
                "presupuesto" => $proyecto->presupuesto,
                "acuenta" => ProyectoPagoCuenta::getTotalAcuentaProyecto($proyecto->id),
                "asignado" => Asignacion::totalAsignateByProyect($proyecto->id),
                "gastos" => Pago::where('deleted', '=', 0)->where('proyecto_id', '=', $proyecto->id)->sum('monto'),
                "fecha_inicio" => $proyecto->fecha_inicio,
                "fecha_fin" => $proyecto->fecha_fin,
                "deleted" => $proyecto->deleted,
                ]);
        }

        return response()->json(
//            Proyecto::all()
            $results
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProyectoPost $request)
    {
        $proyecto = new Proyecto();
        $proyecto->nombre = $request->get('nombre');
        $proyecto->presupuesto = $request->get('presupuesto');
        $proyecto->descripcion = $request->get('descripcion');
        $proyecto->fecha_inicio = $request->get('fecha_inicio');
        $proyecto->fecha_fin = $request->get('fecha_fin');
        $proyecto->user_id = Auth::user()->id;
        $proyecto->save();

        return response()->json(
            $proyecto
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Proyecto::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $proyecto = Proyecto::find($id);
        $proyecto->deleted = 1;
        $proyecto->save();
        return response()->json(['result' => true, 'id' => $id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function assignUserProyecto(Request $request)
    {
        $userId = $request->get('user_id');
        $proyectoId = $request->get('proyecto_id');

        /** @var User $user */
        $user = User::find($userId);

        $ids = [];
        $proyectos = $user->proyectos;
        if(count($proyectos) > 0){
            $bandera = false;
            foreach ($proyectos as $proyecto){
                if($proyecto->id == $proyectoId)
                    $bandera = true;
                array_push($ids, $proyecto->id);
            }
            if(!$bandera)
                array_push($ids, $proyectoId);
        } else {
            array_push($ids, $proyectoId);
        }
        $user->proyectos()->sync($ids);
        return response()->json(['result' => true]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function assignPersonaProyecto(Request $request)
    {
        $personaId = $request->get('persona_id');
        $proyectoId = $request->get('proyecto_id');
        /** @var Persona $persona */
        $persona = Persona::find($personaId);

        $ids = [];
        $proyectos = $persona->proyectos;
        if(count($proyectos) > 0){
//            return response()->json(['result' => $proyectos]);
            $bandera = false;
            foreach ($proyectos as $proyecto){
                if($proyecto->id == $proyectoId)
                    $bandera = true;
                array_push($ids, $proyecto->id);
            }
            if(!$bandera)
                array_push($ids, $proyectoId);
        } else {
            array_push($ids, $proyectoId);
        }
        $persona->proyectos()->sync($ids);
        return response()->json(['result' => true]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function assignManagerProyecto(Request $request)
    {
        $userId = $request->get('user_id');
        $proyectoId = $request->get('proyecto_id');
        /** @var User $user */
        $user = User::find($userId);

        $ids = [];
        $proyectos = $user->proyectos;
        if(count($proyectos) > 0){
//            return response()->json(['result' => $proyectos]);
            $bandera = false;
            foreach ($proyectos as $proyecto){
                if($proyecto->id == $proyectoId)
                    $bandera = true;
                array_push($ids, $proyecto->id);
            }
            if(!$bandera)
                array_push($ids, $proyectoId);
        } else {
            array_push($ids, $proyectoId);
        }
        $user->proyectos()->sync($ids);
        return response()->json(['result' => true]);
    }

    public function getPersonasByProyectoId($proyectoId){
        $personas = Proyecto::find($proyectoId)->personas;
        $pers = [];
        foreach ($personas as $persona){
            $pago = Pago::where('persona_id', '=', $persona->id)->where('proyecto_id', '=', $proyectoId)->where('deleted', '=', 0)->sum('monto');
            array_push($pers, ['id' => $persona->id, 'ci' => $persona->ci,
                'nombres' => $persona->nombres, 'apellido_paterno' => $persona->apellido_paterno,
                'apellido_materno' => $persona->apellido_materno, 'telefono' => $persona->telefono,
                'direccion' => $persona->direccion, 'email' => $persona->email, 'pago_proyecto' => $pago]);
        }
        return response()->json($pers);

//        return response()->json($personas);
    }

    public function getPersonasByProyectoIdAndManagerId($proyectoId, $managerId){
        $personas = Proyecto::find($proyectoId)->personas;
        $pers = [];
        foreach ($personas as $persona){
            $pago = Pago::where('persona_id', '=', $persona->id)->where('user_id', '=', $managerId)->where('proyecto_id', '=', $proyectoId)->where('deleted', '=', 0)->sum('monto');
            array_push($pers, ['id' => $persona->id, 'ci' => $persona->ci,
                'nombres' => $persona->nombres, 'apellido_paterno' => $persona->apellido_paterno,
                'apellido_materno' => $persona->apellido_materno, 'telefono' => $persona->telefono,
                'direccion' => $persona->direccion, 'email' => $persona->email, 'pago_proyecto' => $pago]);
        }
        return response()->json($pers);
    }

    public function getTotalGastos($proyectoId){
        $totalGastos = Pago::where('proyecto_id', '=', $proyectoId)->where('deleted', '=', 0)->sum('monto');
        return response()->json($totalGastos);
    }

    public function getTotalManagerGastos($proyectoId, $managerId){
        $totalGastos = Pago::where('proyecto_id', '=', $proyectoId)->where('user_id', '=', $managerId)->where('deleted', '=', 0)->sum('monto');
        return response()->json($totalGastos);
    }

    public function getManagerByNotProyectoSearch(Request $request, $proyectoId){
        $search = $request->post('search');
//        return response()->json(['search' => $search, 'id' => $proyectoId]);
        /** @var Proyecto $proyecto */
        $proyecto = Proyecto::find($proyectoId);
        $users = $proyecto->users;
        $usersId = [];
        foreach ($users as $user){
            array_push($usersId, $user->id);
        }
//        return response()->json($usersId);
//         echo "xxxx:: ".$proyectoId."||".$search;
//        die();
        $usersResult = User::whereNotIn('id', $usersId)->where('role', '=', User::ROL_MANAGER)->where(
            function($query) use ($search){
                $query->orWhere('name', 'like', '%'.$search.'%');
                $query->orWhere('email', 'like', '%'.$search.'%');
            }
        )->get();
        return response()->json($usersResult);
    }

    public function getManagersByProyectoId($proyectoId){
        $users = Proyecto::find($proyectoId)->users;
        $us = [];
        foreach ($users as $user){
//            $pago = Pago::where('user_id', '=', $user->id)->where('proyecto_id', '=', $proyectoId)->where('deleted', '=', 0)->sum('monto');
            $totalAsignado = Asignacion::totalAsignateByProyectAndManager($proyectoId, $user->id);
            array_push($us, ['id' => $user->id, 'name' => $user->name, 'role' => $user->role,
                'email' => $user->email, 'pago_proyecto' => $totalAsignado]);
        }
        return response()->json($us);
    }

    public function getManagersByNotProyectoSearch(Request $request, $proyectoId){
        $search = $request->post('search');
        /** @var Proyecto $proyecto */
        $proyecto = Proyecto::find($proyectoId);
        $users = $proyecto->users;
        $usersId = [];
        foreach ($users as $user){
            array_push($usersId, $user->id);
        }
//         echo "xxxx:: ".$proyectoId."||".$search;
//        die();
        $usersResult = User::whereNotIn('id', $usersId)->where('role', '<>', User::ROL_ADMIN)->where(
            function($query) use ($search){
                $query->orWhere('name', 'like', '%'.$search.'%');
                $query->orWhere('email', 'like', '%'.$search.'%');
            }
        )->get();
        return response()->json($usersResult);
    }

    public function getProyectosByManager(Request $request){
        $user_id = $request->post('user_id');
        $proyectos = User::find($user_id)->proyectos;

        $results = array();
        foreach ($proyectos as $proyecto){
            array_push($results, [
                "id" => $proyecto->id,
                "nombre" => $proyecto->nombre,
                "descripcion" => $proyecto->descripcion,
                "presupuesto" => $proyecto->presupuesto,
                "gastos" => Pago::where('proyecto_id', '=', $proyecto->id)->sum('monto'),
                "fecha_inicio" => $proyecto->fecha_inicio,
                "fecha_fin" => $proyecto->fecha_fin,
                "deleted" => $proyecto->deleted,
            ]);
        }


        return response()->json(
            $results
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function unassignUserProyecto(Request $request)
    {
        $userId = $request->get('manager_id');
        $proyectoId = $request->get('proyecto_id');

        $total = Asignacion::totalAsignateByProyectAndManager($proyectoId, $userId);
        $valid = false;
        if($total == 0){
            /** @var User $user */
            $user = User::find($userId);

            $ids = [];
            $proyectos = $user->proyectos;
            if(count($proyectos) > 0){
                foreach ($proyectos as $proyecto){
                    if($proyecto->id != $proyectoId){
                        array_push($ids, $proyecto->id);
                    }
                }
            }
            $user->proyectos()->sync($ids);
            $valid = true;
        }

        return response()->json(array('valid' => $valid));

//        if()
//        return response()->json(['result' => true]);
    }
}
