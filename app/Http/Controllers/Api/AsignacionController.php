<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAsignacionPost;
use App\Models\Asignacion;
use App\Models\AsignacionImagen;
use App\Models\Pago;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class AsignacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreAsignacionPost  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAsignacionPost $request)
    {
        $asignasion = new Asignacion();
        $asignasion->proyecto_id = $request->get('proyecto_id');
        $asignasion->manager_id = $request->get('manager_id');
        $asignasion->monto = $request->get('monto');
        $asignasion->fecha = $request->get('fecha');
        $asignasion->descripcion = $request->get('descripcion');
        $asignasion->user_id = Auth::id();
        $asignasion->save();

        $photos = $request->get('photos');

        if($photos){
            foreach ($photos as $index => $photo){
                $data = $photo;
                $datas = explode(",", $data);
                $dataImg = base64_decode($datas[1]);

                $img = imagecreatefromstring($dataImg);
                $fileName = '/img/uploads/asignasiones-imagenes/'.$asignasion->id.'-'.$index.'-'.date('YmdHis').'.jpeg';
                imagejpeg($img, public_path().$fileName);

                $asignasionImagen = new AsignacionImagen();
                $asignasionImagen->url = $fileName;
                $asignasionImagen->asignacion_id = $asignasion->id;
                $asignasionImagen->save();
            }
        }

        return response()->json(['asignacion_id' => $asignasion->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $asignacion = Asignacion::find($id);

        return response()->json(['asignacion' => $asignacion]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//        $asignacionImagenes = AsignacionImagen::where('asignacion_id', '=', $id)->get();
//        foreach ($asignacionImagenes as $asignacionI){
//            unlink(base_path().'/public'.$asignacionI->url);
//        }
//        AsignacionImagen::where('asignacion_id', '=', $id)->delete();
//        return response()->json(Asignacion::destroy($id));
        $asignacion = Asignacion::findOrFail($id);
        $asignacion->deleted = 1;

        return response()->json($asignacion->save());
    }

    public function getAllManagers()
    {
        $managers = User::where('role', '=', User::ROL_SUPERVISOR)->orWhere('role', '=', User::ROL_BUILDER)->orWhere('role', '=', User::ROL_MANAGER)->get();

        return response()->json(['managers' => $managers]);
    }

    public function getTotalAsignateByProyectAndManager(Request $request){
        $proyectoId = $request->get('proyecto_id');
        $managerId = $request->get('manager_id');
        $totalAsignate = Asignacion::totalAsignateByProyectAndManager($proyectoId, $managerId);

        return response()->json(['total' => $totalAsignate]);
    }

    public function getTotalAsignateByProyect(Request $request){
        $proyectoId = $request->get('proyecto_id');
        $totalAsignate = Asignacion::totalAsignateByProyect($proyectoId);

        return response()->json(['total' => $totalAsignate]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function getPagosByProyectoManagerBeetweenDate(Request $request){
        $proyectoId = $request->post('proyecto_id');
        $managerId = $request->post('manager_id');
        $fechaIni = $request->post('fecha_inicio');
        $fechaFin = $request->post('fecha_fin');
//        return response()->json(['result' => $proyectoId ]);
        return response()->json(Asignacion::searchAsignacionByProyectoAndManagerAndFechas(
            $proyectoId, $managerId, $fechaIni, $fechaFin
        ));
    }

    public function getAsignacionImagesById($asignacionId){
        return response()->json(
            AsignacionImagen::where('asignacion_id', '=', $asignacionId)->get());
    }
}
