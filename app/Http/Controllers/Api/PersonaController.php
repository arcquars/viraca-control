<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePersonaPost;
use App\Http\Requests\UpdatePersonaPost;
use App\Models\Persona;
use App\Models\Proyecto;
use Illuminate\Http\Request;

class PersonaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(
            Persona::where('deleted', false)->get()
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StorePersonaPost  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePersonaPost $request)
    {
        $persona = new Persona();
        $this->setProperties($persona, $request);
        $result = $persona->save();

        return response()->json($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(Persona::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePersonaPost $request, $id)
    {
        $persona = Persona::find($id);
        $this->setProperties($persona, $request);
        $result = $persona->save();
        response()->json(['result' => $result]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getPersonByNotProyecto($proyectoId){

        /** @var Proyecto $proyecto */
        $proyecto = Proyecto::find($proyectoId);
        $personas = $proyecto->personas;
        $personasId = [];
        foreach ($personas as $per){
            array_push($personasId, $per->id);
        }
//         echo "xxxx:: ".$proyectoId;
//        die();
        return response()->json(['result' => true, 'personas' => Persona::whereNotIn('id', $personasId)->get()]);
    }

    public function getPersonByNotProyectoSearch(Request $request, $proyectoId){
        $search = $request->post('search');
        /** @var Proyecto $proyecto */
        $proyecto = Proyecto::find($proyectoId);
        $personas = $proyecto->personas;
        $personasId = [];
        foreach ($personas as $per){
            array_push($personasId, $per->id);
        }
//         echo "xxxx:: ".$proyectoId."||".$search;
//        die();
        $personasResult = Persona::whereNotIn('id', $personasId)->where(
            function($query) use ($search){
                $query->orWhere('nombres', 'like', '%'.$search.'%');
                $query->orWhere('apellido_paterno', 'like', '%'.$search.'%');
                $query->orWhere('apellido_materno', 'like', '%'.$search.'%');
            }
        )->get();
        return response()->json($personasResult);
    }

    private function setProperties(&$persona, $request){
        $persona->ci = $request->get('ci');
        $persona->nombres = $request->get('nombres')? strtoupper($request->get('nombres')) : '';
        $persona->apellido_paterno = $request->get('apellido_paterno')? strtoupper($request->get('apellido_paterno')) : '';
        $persona->apellido_materno = $request->get('apellido_materno')? strtoupper($request->get('apellido_materno')) : '';
        $persona->email = $request->get('email');
        $persona->telefono = $request->get('telefono');
        $persona->direccion = $request->get('direccion');
    }
}
