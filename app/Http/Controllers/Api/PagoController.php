<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePagoPost;
use App\Models\Pago;
use App\Models\PagoImagen;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class PagoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePagoPost $request)
    {
        $pago = new Pago();
        $pago->persona_id = $request->get('persona_id');
        $pago->proyecto_id = $request->get('proyecto_id');
        $pago->monto = $request->get('monto');
        $pago->fecha = $request->get('fecha');
        $pago->descripcion = $request->get('descripcion');
        $pago->user_id = Auth::id();
        $pago->save();

        $photos = $request->get('photos');

        if($photos){
            foreach ($photos as $index => $photo){
                $data = $photo;
                $datas = explode(",", $data);
                $dataImg = base64_decode($datas[1]);

                $img = imagecreatefromstring($dataImg);
                $fileName = '/img/uploads/pagos-imagenes/'.$pago->id.'-'.$index.'-'.date('YmdHis').'.jpeg';
                imagejpeg($img, public_path().$fileName);

                $pagoImagen = new PagoImagen();
                $pagoImagen->url = $fileName;
                $pagoImagen->pago_id = $pago->id;
                $pagoImagen->save();
            }
        }

        return response()->json(['pago_id' => $pago->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//        return \response()->json(base_path());
        $pagoImagenes = PagoImagen::where('pago_id', '=', $id)->get();
        foreach ($pagoImagenes as $pagoI){
            unlink(base_path().'/public'.$pagoI->url);
        }
        PagoImagen::where('pago_id', '=', $id)->delete();

        return response()->json(Pago::destroy($id));
    }

    public function getPagosByProyectoPerson($proyectoId ,$personId){
        return response()->json(
            Pago::where('persona_id', '=', $personId)
                ->where('proyecto_id', '=', $proyectoId)->get());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function getPagosByProyectoPersonBeetweenDate(Request $request){
        $proyectoId = $request->post('proyecto_id');
        $personaId = $request->post('persona_id');
        $fechaIni = $request->post('fecha_inicio');
        $fechaFin = $request->post('fecha_fin');
//        return response()->json(['result' => $proyectoId ]);
        return response()->json(
            Pago::where('persona_id', '=', $personaId)
                ->where('proyecto_id', '=', $proyectoId)
                ->whereBetween('fecha', [$fechaIni, $fechaFin])->orderBy('fecha', 'ASC')->get());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function getPagosByProyectoManagerBeetweenDate(Request $request){
        $proyectoId = $request->post('proyecto_id');
        $managerId = $request->post('manager_id');
        $fechaIni = $request->post('fecha_inicio');
        $fechaFin = $request->post('fecha_fin');
//        return response()->json(['result' => $proyectoId ]);
        return response()->json(
            Pago::where('user_id', '=', $managerId)
                ->where('proyecto_id', '=', $proyectoId)
                ->whereBetween('fecha', [$fechaIni, $fechaFin])->orderBy('fecha', 'ASC')->get());
    }

    public function getPagoImagesByPagoId($pagoId){
        return response()->json(
            PagoImagen::where('pago_id', '=', $pagoId)->get());
    }

}
