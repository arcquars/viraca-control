<?php


namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Registro de usuario
     * @param $request Request
     * @return Response
     */
    public function signUp(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string'
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        return response()->json([
            'message' => 'Successfully created user!'
        ], 201);
    }

    /**
     * Inicio de sesión y creación de token
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

//        $credentials = request(['email', 'password']);
//        if (!Auth::attempt($credentials))
        if (!Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password'), 'lock' => 0]))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');

//        $token = $tokenResult->accessToken;
//        if ($request->remember_me)
//            $token->expires_at = Carbon::now()->addWeeks(1);
//        $token->save();

        return response()->json([
            'access_token' => $tokenResult,
            'token_type' => 'Bearer',
            'user_id' => $user->id,
            'role' => $user->role,
            'lock' => $user->lock
//            'expires_at' => Carbon::parse($token->expires_at)->toDateTimeString()
        ]);
    }

    public function getLockByUsername(Request $request){
        $username = $request->post('username');
        $user = User::where('email', '=', $username)->first();
        return response()->json(['lock' => $user->lock]);
    }

    /**
     * Cierre de sesión (anular el token)
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * Obtener el objeto User como json
     */
    public function user(Request $request)
    {
//        $request->user()
        return response()->json($request->user());
    }
}
