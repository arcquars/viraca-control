<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProyectoPost;
use App\Models\Proyecto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class ProyectoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('proyectos.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $estado = Proyecto::ESTADO_NUEVO;
        return view('proyectos.create', compact('estado'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProyectoPost $request)
    {
        $proyecto = new Proyecto();
        $proyecto->nombre = $request->get('nombre');
        $proyecto->presupuesto = $request->get('presupuesto');
        $proyecto->descripcion = $request->descripcion = $request->get('descripcion');
        $proyecto->gastos = 0;
        $proyecto->estado = $request->get('estado');
        $proyecto->fecha_inicio = $request->get('fecha_inicio');
        $proyecto->fecha_fin = $request->get('fecha_fin');
        $proyecto->user_id = Auth::id();
        $proyecto->save();

        return redirect('/admin/proyectos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $proyecto = Proyecto::find($id);
        return view('proyectos.view', compact('proyecto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $estado = Proyecto::ESTADO_NUEVO;
        $proyecto = Proyecto::find($id);

        return view('proyectos.edit', compact('proyecto', 'estado'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  StoreProyectoPost $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreProyectoPost $request, $id)
    {
        $proyecto = Proyecto::find($id);
        $proyecto->nombre = $request->get('nombre');
        $proyecto->presupuesto = $request->get('presupuesto');
        $proyecto->descripcion = $request->descripcion = $request->get('descripcion');
        $proyecto->estado = $request->get('estado');
        $proyecto->fecha_inicio = $request->get('fecha_inicio');
        $proyecto->fecha_fin = $request->get('fecha_fin');
        $proyecto->save();

        return redirect('/admin/proyectos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getProyectos(Request $request){
        if($request->ajax()){
            $data = Proyecto::latest()->get();

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="'.route('proyectos.edit', ['proyecto' => $row->id]).'" class="btn btn-link btn-sm text-primary"><i class="far fa-edit"></i></a> ';
                    $actionBtn .= '<a href="'.route('proyectos.show', ['proyecto' => $row->id]).'" class="btn btn-link btn-sm text-primary"><i class="far fa-eye"></i></a> ';
                    $actionBtn .= '<a href="javascript:void(0)" class="btn btn-link btn-sm text-danger"><i class="far fa-trash-alt"></i></a> ';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }
}
