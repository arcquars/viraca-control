<div class="form-group">
    <label for="email">Correo Electrónico</label>
    <input id="email" type="text" class="form-control form-control-sm @error('email') is-invalid @enderror" name="email"
           value="{{ $user? $user->email : old('email')}}" >
    @error('email')
    <div class="text-danger">{{ $message }}</div>
    @enderror
</div>
<div class="form-group">
    <label for="name">Nombre</label>
    <input id="name" type="text" class="form-control form-control-sm @error('name') is-invalid @enderror" name="name" value="{{ $user? $user->name : old('name') }}" >
    @error('name')
    <div class="text-danger">{{ $message }}</div>
    @enderror
</div>
<div class="form-group">
    <label for="password" class="text-md-right">{{ __('Password') }}</label>
    <input id="password" type="password" class="form-control form-control-sm @error('password') is-invalid @enderror" name="password">
    @error('password')
    <div class="text-danger">{{ $message }}</div>
    @enderror
</div>
<div class="form-group">
    <label for="password-confirm" class="text-md-right">{{ __('Confirm Password') }}</label>
    <input id="password-confirm" type="password" class="form-control form-control-sm" name="password_confirmation">
    @error('password_confirmation')
    <div class="text-danger">{{ $message }}</div>
    @enderror
</div>
<div class="form-group">
    <label for="role">Rol</label>
    <select name="role" class="form-control form-control-sm">
        <option value="">Seleccione</option>
        @foreach($roles as $key => $value)
            <option value="{{$key}}">{{$value}}</option>
        @endforeach
    </select>
    @error('role')
    <div class="text-danger">{{ $message }}</div>
    @enderror
</div>