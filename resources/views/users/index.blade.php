@extends('adminlte::page')

@section('title', 'Usuarios')

@section('content_header')
    <div class="row">
        <div class="col-md-6">
            <h1>Usuarios</h1>
        </div>
        <div class="col-md-6">
            <a href="{{route('users.create')}}" class="btn btn-success btn-sm float-right"><i class="far fa-plus-square"></i> Crear nuevo</a>
        </div>
    </div>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="table-responsive-sm">
                <table id="yajra-datatable-users" class="table table-light table-sm">
                    <thead class="thead-dark">
                    <tr>
                        <th>No</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Roles</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>
        $( document ).ready(function() {
            var table = $('#yajra-datatable-users').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('users.list') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'name', name: 'nombre'},
                    {data: 'email', name: 'Correo'},
                    {data: 'role', name: 'Rol'},
                    {
                        data: 'action',
                        name: 'action',
                        orderable: true,
                        searchable: true
                    },
                ]
            });
        });
    </script>
@stop