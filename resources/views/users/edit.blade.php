@extends('adminlte::page')

@section('title', 'Editar usuario')

@section('content_header')
    <h3>Editar usuario</h3>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="/admin/users/{{$user->id}}" method="POST">
                @method('PUT')
                @csrf
                @include('users.partials._form_user', ['user' => $user])
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <a href="{{url()->previous()}}" class="btn btn-dark">Atras</a>
                        <button type="submit" class="btn btn-primary float-right">Grabar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop

@section('css')
{{--    <link rel="stylesheet" href="/css/admin_custom.css">--}}
@stop

@section('js')
    <script>
        $( document ).ready(function() {

        });
    </script>
@stop