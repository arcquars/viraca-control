@extends('adminlte::page')

@section('title', 'Crear usuario')

@section('content_header')
    <h3>Crear usuario</h3>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="/admin/users" method="POST" autocomplete="off">
                @csrf
                @include('users.partials._form_user', ['user' => null])
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <a href="{{url()->previous()}}" class="btn btn-dark">Atras</a>
                        <button type="submit" class="btn btn-primary float-right">Crear</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop

@section('css')
{{--    <link rel="stylesheet" href="/css/admin_custom.css">--}}
@stop

@section('js')
    <script>
        $( document ).ready(function() {

        });
    </script>
@stop