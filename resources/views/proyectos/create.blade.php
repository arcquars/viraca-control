@extends('adminlte::page')

@section('title', 'Crear proyecto')

@section('content_header')
    <h3>Crear proyecto</h3>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="/admin/proyectos" method="POST">
                @csrf
                @include('proyectos.partials._form_producto', ['proyecto' => null])
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <a href="{{url()->previous()}}" class="btn btn-dark">Atras</a>
                        <button type="submit" class="btn btn-primary float-right">Crear</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop

@section('css')
{{--    <link rel="stylesheet" href="/css/admin_custom.css">--}}
@stop

@section('js')
    <script>
        $( document ).ready(function() {

        });
    </script>
@stop