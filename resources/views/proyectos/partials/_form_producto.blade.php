<div class="row">
    <div class="col-md-6">
        <div class="form-group-sm">
            <label for="nombre" >Nombre <span class="text-danger">*</span></label>
            <input type="text" class="form-control form-control-sm @error('nombre') is-invalid @enderror"
                   name="nombre" value="{{ $proyecto?  $proyecto->nombre : old('nombre') }}">
            @error('nombre')
            <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group-sm">
            <label for="presupuesto" >Presupuesto <span class="text-danger">*</span></label>
            <input type="text" class="form-control form-control-sm @error('presupuesto') is-invalid @enderror"
                   name="presupuesto" value="{{ $proyecto?  $proyecto->presupuesto : old('presupuesto') }}">
            @error('presupuesto')
            <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>
</div>
<div class="form-group-sm">
    <label for="estado" >Estado <span class="text-danger">*</span></label>
    <select name="estado" class="form-control form-control-sm" readonly>
        <option value="{{$estado}}">{{$estado}}</option>
    </select>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group-sm">
            <label for="fecha_inicio" >Fecha de inicio <span class="text-danger">*</span></label>
            <input type="date" class="form-control form-control-sm @error('fecha_inicio') is-invalid @enderror"
                   name="fecha_inicio" value="{{$proyecto?  $proyecto->fecha_inicio->format('Y-m-d') : old('fecha_inicio') }}">
            @error('fecha_inicio')
            <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group-sm">
            <label for="fecha_fin" >Fecha de fin <span class="text-danger">*</span></label>
            <input type="date" class="form-control form-control-sm @error('fecha_fin') is-invalid @enderror"
                   name="fecha_fin" value="{{$proyecto?  $proyecto->fecha_fin->format('Y-m-d') : old('fecha_fin') }}">
        </div>
        @error('fecha_fin')
        <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>
</div>
<div class="form-group-sm">
    <label for="descripcion" >Descripción</label>
    <textarea name="descripcion" class="form-control form-control-sm" style="resize: none;" rows="3">{{ $proyecto? $proyecto->descripcion : old('descripcion') }}</textarea>
</div>