@extends('adminlte::page')

@section('title', 'Ver proyecto')

@section('content_header')
    <h3>Ver proyecto</h3>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <strong>Nombre</strong>
                    <p>{{$proyecto->nombre}}</p>
                </div>
                <div class="col-md-6">
                    <strong>Presupuesto</strong>
                    <p>{{$proyecto->presupuesto}}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <strong>Fecha inicio</strong>
                    <p>{{$proyecto->fecha_inicio->format('Y-m-d')}}</p>
                </div>
                <div class="col-md-6">
                    <strong>Fecha fin</strong>
                    <p>{{$proyecto->fecha_fin->format('Y-m-d')}}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <strong>Estado</strong>
                    <p>{{$proyecto->estado}}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <strong>Descripción</strong>
                    <p>{{$proyecto->descripcion}}</p>
                </div>
            </div>
            <a href="{{url()->previous()}}" class="btn btn-dark btn-sm">Atras</a>
        </div>
    </div>
@stop

@section('css')
{{--    <link rel="stylesheet" href="/css/admin_custom.css">--}}
@stop

@section('js')
    <script>
        $( document ).ready(function() {

        });
    </script>
@stop