@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <div class="row">
        <div class="col-md-6">
            <h1>Proyectos</h1>
        </div>
        <div class="col-md-6">
            <a href="{{route('proyectos.create')}}" class="btn btn-success btn-sm float-right"><i class="far fa-plus-square"></i> Crear nuevo</a>
        </div>
    </div>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="table-responsive-sm">
                <table id="yajra-datatable-proyectos" class="table table-light table-sm">
                    <thead class="thead-dark">
                    <tr>
                        <th>No</th>
                        <th>Nombre</th>
                        <th>Presupuesto</th>
                        <th>Estado</th>
                        <th>F. inicio</th>
                        <th>F. fin</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>
        $( document ).ready(function() {
            var table = $('#yajra-datatable-proyectos').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('proyectos.list') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'nombre', name: 'nombre'},
                    {data: 'presupuesto', name: 'presupuesto'},
                    {data: 'estado', name: 'estado'},
                    {data: 'fecha_inicio', name: 'fecha_inicio'},
                    {data: 'fecha_fin', name: 'fecha_fin'},
                    {
                        data: 'action',
                        name: 'action',
                        orderable: true,
                        searchable: true
                    },
                ]
            });
        });
    </script>
@stop