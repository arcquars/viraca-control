-- MySQL dump 10.13  Distrib 5.7.32, for Linux (x86_64)
--
-- Host: localhost    Database: viraca_control
-- ------------------------------------------------------
-- Server version	5.7.32-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `material_imagenes`
--

DROP TABLE IF EXISTS `material_imagenes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `material_imagenes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `material_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `material_imagenes_material_id_foreign` (`material_id`),
  CONSTRAINT `material_imagenes_material_id_foreign` FOREIGN KEY (`material_id`) REFERENCES `pp_materiales` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `material_imagenes`
--

LOCK TABLES `material_imagenes` WRITE;
/*!40000 ALTER TABLE `material_imagenes` DISABLE KEYS */;
INSERT INTO `material_imagenes` VALUES (1,'/img/uploads/material-imagenes/3-20210122074641.jpeg','2021-01-22 11:46:41','2021-01-22 11:46:41',3),(2,'/img/uploads/material-imagenes/3-20210122074641.jpeg','2021-01-22 11:46:41','2021-01-22 11:46:41',3),(3,'/img/uploads/material-imagenes/4-0-20210126014932.jpeg','2021-01-26 05:49:32','2021-01-26 05:49:32',4),(4,'/img/uploads/material-imagenes/4-1-20210126014933.jpeg','2021-01-26 05:49:33','2021-01-26 05:49:33',4),(6,'/img/uploads/material-imagenes/6-0-20210126030248.jpeg','2021-01-26 07:02:48','2021-01-26 07:02:48',6);
/*!40000 ALTER TABLE `material_imagenes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2014_10_12_200000_add_two_factor_columns_to_users_table',1),(4,'2019_08_19_000000_create_failed_jobs_table',1),(5,'2019_12_14_000001_create_personal_access_tokens_table',1),(6,'2020_10_10_135052_create_sessions_table',1),(12,'2016_06_01_000001_create_oauth_auth_codes_table',2),(13,'2016_06_01_000002_create_oauth_access_tokens_table',2),(14,'2016_06_01_000003_create_oauth_refresh_tokens_table',2),(15,'2016_06_01_000004_create_oauth_clients_table',2),(16,'2016_06_01_000005_create_oauth_personal_access_clients_table',2),(17,'2020_10_11_030826_create_proyectos_table',3),(18,'2020_12_29_051253_add_role_to_users_table',4),(19,'2021_01_04_150026_create_personas_table',5),(20,'2021_01_09_032152_create_project_managers_table',6),(21,'2021_01_09_043915_create_proyecto_persona_table',7),(22,'2021_01_09_070638_add_email_to_persona_table',8),(23,'2021_01_15_022049_create_pp_pagos_table',9),(24,'2021_01_16_020237_add_user_id_to_pp_pagoss_table',10),(25,'2021_01_16_032930_create_pago_imagenes_table',11),(28,'2021_01_22_052928_create_pp_materiales_table',12),(29,'2021_01_22_053432_create_material_imagenes_table',12);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `client_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `client_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_auth_codes`
--

LOCK TABLES `oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` VALUES ('91bc54d1-3e4f-4430-97d4-abfad08c12d0',NULL,'Laravel Personal Access Client','4B88EQKerCW4moC9FEMfYEYIoWzcN6tweOLmsWsM',NULL,'http://localhost',1,0,0,'2020-10-11 05:43:48','2020-10-11 05:43:48'),('91bc54d1-8da7-4e94-bf37-6fe4d45f3ad8',NULL,'Laravel Password Grant Client','m4ENqrjHAhkD37vBhEhyJOuHTqZ3t0Y6TxYauGS4','users','http://localhost',0,1,0,'2020-10-11 05:43:48','2020-10-11 05:43:48'),('91bc5883-c916-4326-a5df-b5c989c0b772',3,'angel','xmqud1NaGOhsjw3GljJOVR3S7M9bf5m2VXBPzLmy',NULL,'http://localhost/auth/callback',0,0,0,'2020-10-11 05:54:09','2020-10-11 05:54:09');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_personal_access_clients`
--

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` VALUES (1,'91bc54d1-3e4f-4430-97d4-abfad08c12d0','2020-10-11 05:43:48','2020-10-11 05:43:48');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pago_imagenes`
--

DROP TABLE IF EXISTS `pago_imagenes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pago_imagenes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `pago_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pago_imagenes_pago_id_foreign` (`pago_id`),
  CONSTRAINT `pago_imagenes_pago_id_foreign` FOREIGN KEY (`pago_id`) REFERENCES `pp_pagos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pago_imagenes`
--

LOCK TABLES `pago_imagenes` WRITE;
/*!40000 ALTER TABLE `pago_imagenes` DISABLE KEYS */;
INSERT INTO `pago_imagenes` VALUES (1,'/img/uploads/pagos-imagenes/4-20210116034545.png','2021-01-16 07:45:46','2021-01-16 07:45:46',4),(2,'/img/uploads/pagos-imagenes/6-20210116035108.png','2021-01-16 07:51:11','2021-01-16 07:51:11',6),(3,'/img/uploads/pagos-imagenes/6-20210116035111.png','2021-01-16 07:51:13','2021-01-16 07:51:13',6),(6,'/img/uploads/pagos-imagenes/10-20210116041451.png','2021-01-16 08:14:54','2021-01-16 08:14:54',10),(7,'/img/uploads/pagos-imagenes/14-0-20210126032850.jpeg','2021-01-26 07:28:50','2021-01-26 07:28:50',14);
/*!40000 ALTER TABLE `pago_imagenes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
INSERT INTO `personal_access_tokens` VALUES (21,'App\\Models\\User',3,'Personal Movil Access Token','df5134f0190bb94d2158a20c56f883bf8ac10191944ff07b1cff4dbbac5ae1a9','[\"*\"]',NULL,'2020-10-11 05:44:44','2020-10-11 05:44:44'),(22,'App\\Models\\User',3,'Personal Movil Access Token','0146755ea8b23383083cb47f0454ab98731dae9f9b22f359bbcc9174d6fd9f55','[\"*\"]',NULL,'2020-10-11 05:51:06','2020-10-11 05:51:06'),(23,'App\\Models\\User',3,'Personal Movil Access Token','efcda5caa0fc1201439211db3ac61c2977110d47ca1ef3c82039d1e260d18080','[\"*\"]',NULL,'2020-10-11 05:56:56','2020-10-11 05:56:56'),(24,'App\\Models\\User',3,'Personal Movil Access Token','2a01f22a674ba24698ec7e2f2cfac9f5f56a219c3954d3499970fa9c91a1b6a8','[\"*\"]',NULL,'2020-10-11 06:08:47','2020-10-11 06:08:47'),(25,'App\\Models\\User',3,'Personal Movil Access Token','0278f610624bf94434f33190ad8605d485da47b47ad04092dcca83cadbb95553','[\"*\"]',NULL,'2020-10-11 06:13:14','2020-10-11 06:13:14'),(26,'App\\Models\\User',3,'Personal Movil Access Token','9fcae5c05abf5ad554e47f53f1fd728ea4190c8b203d6f85bf9f54cab07ad2b0','[\"*\"]',NULL,'2020-10-11 06:14:11','2020-10-11 06:14:11'),(27,'App\\Models\\User',3,'Personal Access Token','39a3cfd5498e22192990754094816414540fc0c664efdae3af609d28ad06fb1e','[\"*\"]',NULL,'2020-10-11 06:20:04','2020-10-11 06:20:04'),(28,'App\\Models\\User',3,'Personal Access Token','e66b6d3120ed296d9b2351dcae7d82f32686c17f2dc2887df39c126e74bf7a29','[\"*\"]',NULL,'2020-10-11 06:21:37','2020-10-11 06:21:37'),(29,'App\\Models\\User',3,'Personal Access Token','669679e405493cbff463a64319f46791024dcd23ad92dd31809d9eec394c7266','[\"*\"]','2021-01-22 09:16:20','2020-10-11 06:49:24','2021-01-22 09:16:20'),(30,'App\\Models\\User',3,'Personal Access Token','04d877ac216b4c88506d6bb455005d2cc54bcf3d54fc00dd0dec3326e77a1511','[\"*\"]',NULL,'2020-10-14 05:24:39','2020-10-14 05:24:39'),(31,'App\\Models\\User',3,'Personal Access Token','39ef778ff54d932b43a50777eb2e8a134b70997d443554a18d6bb03ef1194216','[\"*\"]',NULL,'2020-10-14 06:46:50','2020-10-14 06:46:50'),(32,'App\\Models\\User',3,'Personal Access Token','44d23d8544d3c90e2280c2853273a047a15523aef69e30f614a24fe5e4b3b6a1','[\"*\"]',NULL,'2020-10-14 06:50:40','2020-10-14 06:50:40'),(33,'App\\Models\\User',3,'Personal Access Token','3d96cbcfbac01fc124faf90d077f1804406f8d2d9486a4f518e74e21ff970a9f','[\"*\"]',NULL,'2020-10-14 06:54:59','2020-10-14 06:54:59'),(34,'App\\Models\\User',3,'Personal Access Token','559f8beb12a3235ba7bf3164ae4455c0406b1d1920353ca0c35aa2849e3c0e62','[\"*\"]',NULL,'2020-10-14 06:57:01','2020-10-14 06:57:01'),(35,'App\\Models\\User',3,'Personal Access Token','5c2ec774a87c117e0488ff587b45a1daa17ac09552e26407ab201d555e3ad83f','[\"*\"]',NULL,'2020-10-14 07:12:23','2020-10-14 07:12:23'),(36,'App\\Models\\User',3,'Personal Access Token','c64912bf6082dd4e37633d42f24095332977cc8e44e84bdd8e4635dd6728c363','[\"*\"]',NULL,'2020-10-14 07:38:37','2020-10-14 07:38:37'),(37,'App\\Models\\User',3,'Personal Access Token','14d74c670f9c1c29920d342096c4889fbaa36d5f6234ae59fce4d323dabccf99','[\"*\"]',NULL,'2020-10-15 07:22:17','2020-10-15 07:22:17'),(38,'App\\Models\\User',3,'Personal Access Token','2fbfe6291cc3c53732a608b471844eb865ac8e114e8902f88af1c0256b612d74','[\"*\"]',NULL,'2020-10-15 07:38:37','2020-10-15 07:38:37'),(39,'App\\Models\\User',3,'Personal Access Token','aae6d69375cd9df9393e890a5eb7f1bf65669db1715c478c3f6fa46a12799b31','[\"*\"]',NULL,'2020-10-16 06:36:00','2020-10-16 06:36:00'),(40,'App\\Models\\User',3,'Personal Access Token','a79898eb5d25097eb8a591ed97b4b05ee4dd825e68dfd02b7b8dbf5b88565485','[\"*\"]','2020-10-16 07:45:53','2020-10-16 06:42:00','2020-10-16 07:45:53'),(41,'App\\Models\\User',3,'Personal Access Token','e9e1001d56d128105ea7cf6b75ab6ef5e7240a00061008d3d5a7472bc3556109','[\"*\"]',NULL,'2020-10-19 05:03:34','2020-10-19 05:03:34'),(42,'App\\Models\\User',3,'Personal Access Token','c65b0d71606be7157abbb4e513b6773652f8f23c8324eccbe8e42627b2c82df7','[\"*\"]','2021-01-30 09:50:54','2020-10-19 05:04:30','2021-01-30 09:50:54'),(43,'App\\Models\\User',1,'Personal Access Token','e169701380339374f5c67c9b26b669757bd3caa6fbe7ad200129c140abac8c40','[\"*\"]','2021-01-09 06:54:00','2021-01-03 20:09:11','2021-01-09 06:54:00'),(44,'App\\Models\\User',3,'Personal Access Token','1cf2f2d3a12fe99efb95bc1e889930d58120ba8cceef47f66fe8f4fd35b721a7','[\"*\"]',NULL,'2021-01-04 18:06:26','2021-01-04 18:06:26'),(45,'App\\Models\\User',3,'Personal Access Token','6391c09cbe91626ed4c29d870aa92db6e58258c2ab72141c78e074f32c7dbc8b','[\"*\"]','2021-01-30 09:41:26','2021-01-04 18:15:15','2021-01-30 09:41:26'),(46,'App\\Models\\User',1,'Personal Access Token','1ed809a72a5998c9a51e755e34fcdeb2315da1f432699ef635291f88556fca72','[\"*\"]','2021-01-22 11:46:41','2021-01-09 06:55:19','2021-01-22 11:46:41'),(47,'App\\Models\\User',1,'Personal Access Token','10d3716d3f3e4e03e1b96e2247ef024ae5a877a8ff0d42d806d1ef124ff27714','[\"*\"]','2021-01-30 05:36:46','2021-01-26 05:47:51','2021-01-30 05:36:46'),(48,'App\\Models\\User',1,'Personal Access Token','332f11197a93febae61f4c5fc3c68c982a1beabdb3743aeb83f6e031125448e3','[\"*\"]','2021-01-30 06:17:08','2021-01-30 06:17:07','2021-01-30 06:17:08'),(49,'App\\Models\\User',1,'Personal Access Token','d701c55f457deacd16a9abf2b44de2be5a078cf83a75304e76ecec218ff35e9d','[\"*\"]','2021-01-30 06:32:04','2021-01-30 06:17:30','2021-01-30 06:32:04'),(50,'App\\Models\\User',1,'Personal Access Token','e3069857a05947c6db40994e18482ba6a53a8565cd27f404e612e93aca1bc752','[\"*\"]','2021-01-30 06:40:49','2021-01-30 06:40:24','2021-01-30 06:40:49'),(51,'App\\Models\\User',1,'Personal Access Token','22a99720bd41f65cacd84bc5abc31d321d0af35ff826cde0625e402ef5fbb342','[\"*\"]','2021-01-30 06:53:37','2021-01-30 06:41:19','2021-01-30 06:53:37'),(52,'App\\Models\\User',1,'Personal Access Token','1559e28f48b72664d44e4082ff9365e421eb52c6e3512c244141c516dbd06cf5','[\"*\"]',NULL,'2021-01-30 07:04:28','2021-01-30 07:04:28'),(53,'App\\Models\\User',1,'Personal Access Token','4ca9b29a62e5c486c4328b8f44f996ac09538c90507ea505aebc80ed609b657e','[\"*\"]',NULL,'2021-01-30 07:04:36','2021-01-30 07:04:36'),(54,'App\\Models\\User',1,'Personal Access Token','9e9543e8f71e5a7d930ca2df9db0968a5a695ae4bcd7f0d9c5e73e300c7cd685','[\"*\"]',NULL,'2021-01-30 07:04:46','2021-01-30 07:04:46'),(55,'App\\Models\\User',1,'Personal Access Token','69a3abe7b0b6352dc815dbc99b7cafb8f70969f3863a2461cfac7845ba6f6067','[\"*\"]',NULL,'2021-01-30 07:04:57','2021-01-30 07:04:57'),(56,'App\\Models\\User',1,'Personal Access Token','2a293689edb93108d5712967c7ef6fb7be188214e166554e6efff46a90c6dc37','[\"*\"]',NULL,'2021-01-30 07:05:19','2021-01-30 07:05:19'),(57,'App\\Models\\User',1,'Personal Access Token','1884719ef7906ab08466627766fca23de9b915e6ccbcecbe5be907f8d39568d1','[\"*\"]',NULL,'2021-01-30 07:05:40','2021-01-30 07:05:40'),(58,'App\\Models\\User',1,'Personal Access Token','cf2f2a7fc3ea1a5c8d0094a40fb86a504dc61948c33c55cfe622ad87e505968b','[\"*\"]',NULL,'2021-01-30 07:05:53','2021-01-30 07:05:53'),(59,'App\\Models\\User',1,'Personal Access Token','309666ae2da1eba668a1d38bceccb10d784783a7f5b7ab0c2560a0f9447e2a89','[\"*\"]','2021-01-30 07:10:37','2021-01-30 07:06:29','2021-01-30 07:10:37'),(60,'App\\Models\\User',1,'Personal Access Token','43cc2a808c1b2c18c71da1d99afc29b00fc713dd7d25a5f44f3cb71596e014d6','[\"*\"]','2021-01-30 07:11:00','2021-01-30 07:10:59','2021-01-30 07:11:00'),(61,'App\\Models\\User',1,'Personal Access Token','fcbefbeea24ace9b57a98ad3d503fb903f9e89491d353583b464eeea56b84860','[\"*\"]',NULL,'2021-01-30 07:12:18','2021-01-30 07:12:18'),(62,'App\\Models\\User',1,'Personal Access Token','f8413c3c0a0c5aef0a9ab913a7f652a0e2dade91fc011be2ec9d68e6b0504713','[\"*\"]',NULL,'2021-01-30 07:12:20','2021-01-30 07:12:20'),(63,'App\\Models\\User',1,'Personal Access Token','c2d698f31a44f92a6741865f467c461e59354259e795e54e3ee940fbbef8e6c3','[\"*\"]',NULL,'2021-01-30 07:12:25','2021-01-30 07:12:25'),(64,'App\\Models\\User',1,'Personal Access Token','d1ad2f63a00e15fd9b215154b3627d4396ef22b1a743535fc02b5ee067a74fbc','[\"*\"]',NULL,'2021-01-30 07:12:31','2021-01-30 07:12:31'),(65,'App\\Models\\User',1,'Personal Access Token','b73dfeba0ca9526a938a26e7a1b4cd13e58ba741b8b14b8e08b0a7c99c849e97','[\"*\"]',NULL,'2021-01-30 07:14:37','2021-01-30 07:14:37'),(66,'App\\Models\\User',1,'Personal Access Token','1c4566909698b6b03f50b40872ca5eec0a24caff69b1f00922ba98a71b3d0caf','[\"*\"]',NULL,'2021-01-30 07:14:41','2021-01-30 07:14:41'),(67,'App\\Models\\User',1,'Personal Access Token','c9c34777ee73a998a628f0410b88262e47d991474f748bd7afb2a550442ef9f0','[\"*\"]',NULL,'2021-01-30 07:17:22','2021-01-30 07:17:22'),(68,'App\\Models\\User',1,'Personal Access Token','720ec6561d981f6f454c25c61dde80aac4aadf534c49e494c00fe88366ffbc8d','[\"*\"]','2021-01-30 07:26:44','2021-01-30 07:24:51','2021-01-30 07:26:44'),(69,'App\\Models\\User',1,'Personal Access Token','59156211fb4126e83eb0e3aab57db0aecc4f30193e020934123f7be2f97f03f1','[\"*\"]','2021-01-30 07:27:13','2021-01-30 07:26:58','2021-01-30 07:27:13'),(70,'App\\Models\\User',1,'Personal Access Token','c7d20c0c8b06fd1ad506576a625ee27229d40974a63a92ba951f655ea0295712','[\"*\"]','2021-01-30 07:27:38','2021-01-30 07:27:37','2021-01-30 07:27:38'),(71,'App\\Models\\User',1,'Personal Access Token','2e53771d82d9175b33c4adab6104704893d06787da8de0e163f03d2ad25f037f','[\"*\"]',NULL,'2021-01-30 07:39:51','2021-01-30 07:39:51'),(72,'App\\Models\\User',1,'Personal Access Token','00ed38d342293e37a1f1e73645e8c2e9538262c9caae2e91ffc65e97b0ebab5e','[\"*\"]',NULL,'2021-01-30 07:39:56','2021-01-30 07:39:56'),(73,'App\\Models\\User',1,'Personal Access Token','cc167a9e9d5af2e6bb6c08bb7ac6d3fd6d56ba89bfd4a37b2e90e3ba488b3c55','[\"*\"]',NULL,'2021-01-30 07:40:06','2021-01-30 07:40:06'),(74,'App\\Models\\User',1,'Personal Access Token','910efe7c1a4130c4c48469a63e13ec7f0268bae64a18bcc3591eae8c74c47226','[\"*\"]',NULL,'2021-01-30 07:40:07','2021-01-30 07:40:07'),(75,'App\\Models\\User',1,'Personal Access Token','3858b560fdde43635d3d8f53bd8d928db0639e79282cc2cf363d7eee16efa7e6','[\"*\"]',NULL,'2021-01-30 07:40:07','2021-01-30 07:40:07'),(76,'App\\Models\\User',1,'Personal Access Token','831180d9ba1c180307aa025d61d14c9eb7ef4d40902b019f39e07320414408a4','[\"*\"]',NULL,'2021-01-30 07:40:08','2021-01-30 07:40:08'),(77,'App\\Models\\User',1,'Personal Access Token','7f3733042fa311fb7389a464be611cf4ed387f396336800ffd8df80bc8531f73','[\"*\"]',NULL,'2021-01-30 07:40:08','2021-01-30 07:40:08'),(78,'App\\Models\\User',1,'Personal Access Token','ddcba0ed6d8ac93b21042c92fcfc6367af6a5a19acfdd9d28a94b4c4775bf4ee','[\"*\"]',NULL,'2021-01-30 07:40:08','2021-01-30 07:40:08'),(79,'App\\Models\\User',1,'Personal Access Token','100221bdea89e7bc7aac8f42dfe4e46094d05a66964fde2dc8df6b7d6fda1eec','[\"*\"]',NULL,'2021-01-30 07:40:09','2021-01-30 07:40:09'),(80,'App\\Models\\User',1,'Personal Access Token','555e1635d33fff9c6d4fd3cf83b9b4ccdc15f6c43995d3390a4eababe7f1c942','[\"*\"]',NULL,'2021-01-30 07:40:09','2021-01-30 07:40:09'),(81,'App\\Models\\User',1,'Personal Access Token','c9b43abad757f2f5753d4c9b5470d26ccdab8416bf8d344d5ffe3d141ffca259','[\"*\"]','2021-01-30 07:41:57','2021-01-30 07:40:09','2021-01-30 07:41:57'),(82,'App\\Models\\User',1,'Personal Access Token','1ee5143f6f5a3f4526927a8d63c0db37db3625c15e24a37d2b29b90a2c470ff6','[\"*\"]','2021-01-30 07:42:14','2021-01-30 07:42:14','2021-01-30 07:42:14'),(83,'App\\Models\\User',1,'Personal Access Token','a2917560dbe39e088f24cc31b6d5d492d0fb80c842862acbf2a6425468e93365','[\"*\"]','2021-01-30 07:44:16','2021-01-30 07:44:16','2021-01-30 07:44:16'),(84,'App\\Models\\User',1,'Personal Access Token','145d1dd3bf460a0a9574ac4bd588d2af11333e9b901deae6ed78e5e1cab41941','[\"*\"]','2021-01-30 07:45:33','2021-01-30 07:45:32','2021-01-30 07:45:33'),(85,'App\\Models\\User',1,'Personal Access Token','a470a20141a6309725cd8041785c6d209bcfbaf9953bd1eec322c143280e24f1','[\"*\"]','2021-01-30 07:47:21','2021-01-30 07:47:20','2021-01-30 07:47:21'),(86,'App\\Models\\User',1,'Personal Access Token','9885e7e0410ae4ab3917f2f3ee3a3ffb62df8e0a65013ccf35632f90c530c6b6','[\"*\"]','2021-01-30 07:49:15','2021-01-30 07:49:14','2021-01-30 07:49:15'),(87,'App\\Models\\User',1,'Personal Access Token','f3b29e137d565fd6e83e526d434ce7ad0b13954daf3480e6c633da4c8cc0cc10','[\"*\"]','2021-01-30 07:51:50','2021-01-30 07:51:50','2021-01-30 07:51:50'),(88,'App\\Models\\User',1,'Personal Access Token','c38709e9360ba4f6180a5848e99ec17a860db6b5906c80db53b797936487e149','[\"*\"]','2021-01-30 08:07:15','2021-01-30 08:07:15','2021-01-30 08:07:15'),(89,'App\\Models\\User',1,'Personal Access Token','21f513a1cec06a3c1b31d1e6cd0560baec814257e41f0d529117d832e42f5912','[\"*\"]','2021-01-30 08:09:20','2021-01-30 08:09:19','2021-01-30 08:09:20'),(90,'App\\Models\\User',1,'Personal Access Token','daec9c9233f1bb46b39d005a41bbf5082ce4225bb69b51028d88321f890831f8','[\"*\"]','2021-01-30 08:12:11','2021-01-30 08:12:10','2021-01-30 08:12:11'),(91,'App\\Models\\User',1,'Personal Access Token','b268ffff4b3105083fb9f28b680d7c06bbbd81106cc498c8f9d962cc066a4aae','[\"*\"]','2021-01-30 08:26:37','2021-01-30 08:12:28','2021-01-30 08:26:37'),(92,'App\\Models\\User',1,'Personal Access Token','1d13e58df7f7c33d610255fb9ab4898659f1a2f0c16659bcb79ba36eec63ff37','[\"*\"]','2021-01-30 10:29:47','2021-01-30 08:27:09','2021-01-30 10:29:47'),(93,'App\\Models\\User',3,'Personal Access Token','e60e0b486cb3936f86419434dffa33d839a3faf7da5c84b9adb094e3c13997f8','[\"*\"]',NULL,'2021-01-30 09:52:48','2021-01-30 09:52:48'),(94,'App\\Models\\User',3,'Personal Access Token','c0654d96acb1fb0c9f40c379de6037c2e186841b63b07d18a6a3d37782d9001e','[\"*\"]',NULL,'2021-01-30 09:57:50','2021-01-30 09:57:50'),(95,'App\\Models\\User',3,'Personal Access Token','69011adfebd63ef6ea912eebd793d2c3390884a9bda2ca941daae58b4de170cf','[\"*\"]',NULL,'2021-01-30 09:58:25','2021-01-30 09:58:25'),(96,'App\\Models\\User',3,'Personal Access Token','999de40ab7d510944e893c82a82b38da4ad8ea95a442abeb5ec4a9380b12537c','[\"*\"]',NULL,'2021-01-30 09:58:53','2021-01-30 09:58:53'),(97,'App\\Models\\User',3,'Personal Access Token','dc06d8fce0372abd49306a65e2d8d003d40007b52f39851536ee5ea61965e32e','[\"*\"]',NULL,'2021-01-30 10:03:59','2021-01-30 10:03:59'),(98,'App\\Models\\User',3,'Personal Access Token','85eca1f17fc3cea4214fea9f4b47b7e06ffdfa8cc8d3d4c6096ed8c5124d2c54','[\"*\"]',NULL,'2021-01-30 10:05:20','2021-01-30 10:05:20'),(99,'App\\Models\\User',3,'Personal Access Token','757e79497896696a0e84222dd9acfb696321ffda442e6700295079c98a0bb1ce','[\"*\"]',NULL,'2021-01-30 10:10:17','2021-01-30 10:10:17'),(100,'App\\Models\\User',3,'Personal Access Token','1caf110471a58a1e730096af95075b901a3aa5bf7301576867222750578a98e6','[\"*\"]',NULL,'2021-01-30 10:10:30','2021-01-30 10:10:30'),(101,'App\\Models\\User',3,'Personal Access Token','526c9aeeed1f8f9044a7767ab428a70f2b22f95dff41d6b71813509a10cc5291','[\"*\"]',NULL,'2021-01-30 10:11:16','2021-01-30 10:11:16'),(102,'App\\Models\\User',3,'Personal Access Token','1b25cf55df8371733a1edd37be8782b32d57a6c23fb36310e3a8cc5d04ac1dec','[\"*\"]',NULL,'2021-01-30 10:11:28','2021-01-30 10:11:28'),(103,'App\\Models\\User',3,'Personal Access Token','a5686e58018851174997a6e1dda2bda9390bd2e12b2b3fc38193389cbf9bfece','[\"*\"]',NULL,'2021-01-30 10:15:52','2021-01-30 10:15:52'),(104,'App\\Models\\User',3,'Personal Access Token','7c3fac043336154c6a97bc476ed29f49485c2891813a6091828d254e80651e17','[\"*\"]','2021-01-30 10:40:08','2021-01-30 10:30:13','2021-01-30 10:40:08'),(105,'App\\Models\\User',1,'Personal Access Token','acdb732a89f3ce792ffe896af73cd5f0c9fcc0a972eeb84d26a966f0badb1847','[\"*\"]','2021-01-30 10:40:20','2021-01-30 10:40:19','2021-01-30 10:40:20'),(106,'App\\Models\\User',3,'Personal Access Token','c83edbf3e7a1816e04a4717bf01ad32dcb5c32155e27dc51f949c0bfddefa4ad','[\"*\"]','2021-01-30 10:40:44','2021-01-30 10:40:43','2021-01-30 10:40:44'),(107,'App\\Models\\User',1,'Personal Access Token','f48d85eb6094167cb4942bdf7d325e6ad72211d49b58974156e7185bfe567d81','[\"*\"]','2021-01-30 10:46:20','2021-01-30 10:41:29','2021-01-30 10:46:20'),(108,'App\\Models\\User',1,'Personal Access Token','f3b90275b6eb5b131d10b40fe6a37dd10344d7032bb86e79bd85ceae8502ac44','[\"*\"]','2021-01-30 10:48:33','2021-01-30 10:46:38','2021-01-30 10:48:33'),(109,'App\\Models\\User',3,'Personal Access Token','e79de46f4ddab44329e12e11b86bbf733c1d719608679fa9e6cdd720256f8b72','[\"*\"]','2021-01-30 10:56:15','2021-01-30 10:48:48','2021-01-30 10:56:15'),(110,'App\\Models\\User',1,'Personal Access Token','33cb725fb429978960dd1280f737cfa292cbeb01382b0d327fd42b340cb50279','[\"*\"]','2021-01-30 11:04:19','2021-01-30 10:56:48','2021-01-30 11:04:19'),(111,'App\\Models\\User',3,'Personal Access Token','cf6c49939041cea19b514816780b1fda4ac4716b9bdd9c91ecc6673aab2e1125','[\"*\"]','2021-01-30 11:04:40','2021-01-30 11:04:39','2021-01-30 11:04:40'),(112,'App\\Models\\User',1,'Personal Access Token','34c6de4e61508842055439ba3168c6c1f169f1574b1e9c6e13928fbdce93d072','[\"*\"]','2021-01-30 11:05:11','2021-01-30 11:05:01','2021-01-30 11:05:11'),(113,'App\\Models\\User',3,'Personal Access Token','7aa630ccddf027dbe4aa56df6c39ef19b1adcca73cb5764ca214c1425820942c','[\"*\"]','2021-01-30 11:16:31','2021-01-30 11:05:29','2021-01-30 11:16:31'),(114,'App\\Models\\User',1,'Personal Access Token','ee76f081ea38d98aa615b378a7309f4274c3a1cb7cf62fd3e5029c13b7ae3809','[\"*\"]','2021-01-30 11:18:12','2021-01-30 11:16:43','2021-01-30 11:18:12'),(115,'App\\Models\\User',3,'Personal Access Token','f9d08fc8e35bc2f128d8cc1c53765e31a22ee0a71e9a096c780140b1b41859c0','[\"*\"]','2021-01-30 11:18:39','2021-01-30 11:18:38','2021-01-30 11:18:39'),(116,'App\\Models\\User',1,'Personal Access Token','0305f080e21dfe548aab9e749ab716312e0ff50fd5755530d306fbff01838143','[\"*\"]','2021-01-30 11:18:52','2021-01-30 11:18:52','2021-01-30 11:18:52');
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personas`
--

DROP TABLE IF EXISTS `personas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ci` bigint(20) DEFAULT NULL,
  `nombres` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellido_paterno` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellido_materno` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direccion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personas`
--

LOCK TABLES `personas` WRITE;
/*!40000 ALTER TABLE `personas` DISABLE KEYS */;
INSERT INTO `personas` VALUES (1,45613,'JUAN','PEREZ','MARCIO','7531313','Av. GUILLERMO URQUIDI #234',0,NULL,NULL,NULL),(2,1354564,'MARTHAED','DFFFRFG','GUMICIO','4568431','c. MARTIN CARDENAS',0,NULL,'2021-01-28 07:25:18','dgvv@dff.com'),(3,456143,'JUAN CARLOS','GUILLEN','','5789766',NULL,0,'2021-01-05 07:20:26','2021-01-09 11:11:44','aghj@jjuu.vj'),(4,849686,'JUAN CARLOS','OLMOS','MARCIAL','79965567','AV CABILDO 35',0,'2021-01-08 19:19:18','2021-01-08 19:19:18',NULL),(5,NULL,'XAMILI','FERNADEZ','MORCO','1345689','dfh xdfc ff',0,'2021-01-09 11:20:23','2021-01-09 11:21:36','argf@sf.ch');
/*!40000 ALTER TABLE `personas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pp_materiales`
--

DROP TABLE IF EXISTS `pp_materiales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pp_materiales` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `persona_id` bigint(20) NOT NULL,
  `proyecto_id` bigint(20) NOT NULL,
  `cantidad` double(8,2) NOT NULL,
  `fecha` date NOT NULL,
  `unidad` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci,
  `user_id` bigint(20) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pp_materiales`
--

LOCK TABLES `pp_materiales` WRITE;
/*!40000 ALTER TABLE `pp_materiales` DISABLE KEYS */;
INSERT INTO `pp_materiales` VALUES (1,1,2,53.00,'2021-01-22','hf','jjj hjj',1,0,'2021-01-22 11:29:28','2021-01-22 11:29:28'),(2,1,2,32.00,'2021-01-22','gj','fg hgv gg cf. vgcv',1,0,'2021-01-22 11:44:49','2021-01-22 11:44:49'),(3,1,2,5.00,'2021-01-20','xf','dzf',1,0,'2021-01-22 11:46:41','2021-01-22 11:46:41'),(4,2,2,45.00,'2021-01-24','metros','tela microfibra',1,0,'2021-01-26 05:49:32','2021-01-26 05:49:32'),(6,2,2,5.00,'2021-01-25','cc','etc fgg',1,0,'2021-01-26 07:02:47','2021-01-26 07:02:47');
/*!40000 ALTER TABLE `pp_materiales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pp_pagos`
--

DROP TABLE IF EXISTS `pp_pagos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pp_pagos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `persona_id` bigint(20) NOT NULL,
  `proyecto_id` bigint(20) NOT NULL,
  `monto` double(8,2) NOT NULL,
  `fecha` date NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pp_pagos`
--

LOCK TABLES `pp_pagos` WRITE;
/*!40000 ALTER TABLE `pp_pagos` DISABLE KEYS */;
INSERT INTO `pp_pagos` VALUES (1,3,2,450.50,'2021-01-22','sdfdsfsdf fds fsd fsd fdsf dsf',0,'2021-01-15 07:25:40','2021-01-15 07:25:40',1),(2,1,2,55.00,'2021-01-15','dfh ggb',0,'2021-01-15 08:20:11','2021-01-15 08:20:11',1),(3,1,3,558.00,'2020-12-15','fjg hgd bgf hh',0,'2021-01-16 06:23:28','2021-01-16 06:23:28',1),(4,3,2,450.50,'2021-01-22','sdfdsfsdf fds fsd fsd fdsf dsf',0,'2021-01-16 07:45:45','2021-01-16 07:45:45',3),(5,3,2,568.00,'2021-01-15','fgjg gh hh',0,'2021-01-16 07:49:32','2021-01-16 07:49:32',1),(6,3,2,558.00,'2021-01-15','dfh ggv r',0,'2021-01-16 07:51:08','2021-01-16 07:51:08',1),(9,2,2,350.00,'2020-12-16','Algo 1',0,'2021-01-16 08:09:45','2021-01-16 08:09:45',1),(10,2,2,385.00,'2021-01-16','Algo 2',0,'2021-01-16 08:14:51','2021-01-16 08:14:51',1),(11,1,3,520.00,'2021-01-25','adel',0,'2021-01-26 07:15:57','2021-01-26 07:15:57',1),(12,1,3,250.00,'2021-01-25','frt ff',0,'2021-01-26 07:17:03','2021-01-26 07:17:03',1),(13,1,3,650.00,'2021-01-25','df fv',0,'2021-01-26 07:19:07','2021-01-26 07:19:07',1),(14,1,3,425.00,'2021-01-24','rata',0,'2021-01-26 07:28:50','2021-01-26 07:28:50',1),(15,2,2,450.00,'2021-01-26',NULL,0,'2021-01-28 07:38:32','2021-01-28 07:38:32',1),(16,2,3,550.00,'2021-01-29','adelantó salario febrero',0,'2021-01-30 05:36:26','2021-01-30 05:36:26',1);
/*!40000 ALTER TABLE `pp_pagos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_managers`
--

DROP TABLE IF EXISTS `project_managers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_managers` (
  `user_id` bigint(20) unsigned NOT NULL,
  `proyecto_id` bigint(20) unsigned NOT NULL,
  KEY `project_managers_user_id_foreign` (`user_id`),
  KEY `project_managers_proyecto_id_foreign` (`proyecto_id`),
  CONSTRAINT `project_managers_proyecto_id_foreign` FOREIGN KEY (`proyecto_id`) REFERENCES `proyectos` (`id`) ON DELETE CASCADE,
  CONSTRAINT `project_managers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_managers`
--

LOCK TABLES `project_managers` WRITE;
/*!40000 ALTER TABLE `project_managers` DISABLE KEYS */;
INSERT INTO `project_managers` VALUES (4,3),(4,2),(3,2);
/*!40000 ALTER TABLE `project_managers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proyecto_persona`
--

DROP TABLE IF EXISTS `proyecto_persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proyecto_persona` (
  `persona_id` bigint(20) unsigned NOT NULL,
  `proyecto_id` bigint(20) unsigned NOT NULL,
  KEY `proyecto_persona_persona_id_foreign` (`persona_id`),
  KEY `proyecto_persona_proyecto_id_foreign` (`proyecto_id`),
  CONSTRAINT `proyecto_persona_persona_id_foreign` FOREIGN KEY (`persona_id`) REFERENCES `personas` (`id`) ON DELETE CASCADE,
  CONSTRAINT `proyecto_persona_proyecto_id_foreign` FOREIGN KEY (`proyecto_id`) REFERENCES `proyectos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proyecto_persona`
--

LOCK TABLES `proyecto_persona` WRITE;
/*!40000 ALTER TABLE `proyecto_persona` DISABLE KEYS */;
INSERT INTO `proyecto_persona` VALUES (2,2),(2,3),(3,1),(4,1),(2,1),(3,2),(3,4),(1,3),(4,3);
/*!40000 ALTER TABLE `proyecto_persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proyectos`
--

DROP TABLE IF EXISTS `proyectos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proyectos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci,
  `presupuesto` double(10,2) NOT NULL DEFAULT '0.00',
  `gastos` double(10,2) NOT NULL DEFAULT '0.00',
  `estado` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_fin` date DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `proyectos_user_id_foreign` (`user_id`),
  CONSTRAINT `proyectos_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proyectos`
--

LOCK TABLES `proyectos` WRITE;
/*!40000 ALTER TABLE `proyectos` DISABLE KEYS */;
INSERT INTO `proyectos` VALUES (1,'Proyecto 1','It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy).',5800.00,0.00,'NUEVO','2020-10-11','2020-11-30',0,NULL,NULL,3),(2,'Proyecto 2','It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy).',6800.00,0.00,'NUEVO','2020-10-15','2020-12-20',0,NULL,NULL,3),(3,'Proyecto 37','It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy).',7800.00,0.00,'NUEVO','2020-10-21','2020-11-30',0,NULL,NULL,3),(4,'Proyecto web 1','Descripcion para explicar el proyecto',7800.00,0.00,'NUEVO','2020-12-14','2021-01-10',0,'2020-12-31 02:13:55','2020-12-31 02:13:55',1),(5,'Proyecto web 2',NULL,8500.00,0.00,'NUEVO','2020-12-01','2021-01-01',0,'2020-12-31 02:14:49','2020-12-31 02:14:49',1),(6,'Proyecto web 3.1','descripcion 3.1',7900.00,0.00,'NUEVO','2020-12-14','2020-12-15',0,'2020-12-31 07:48:46','2020-12-31 07:55:17',1);
/*!40000 ALTER TABLE `proyectos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sessions_user_id_index` (`user_id`),
  KEY `sessions_last_activity_index` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES ('hLj4XBImWVkUJwFHVzLkDwaSJcd5UcGzihjyCqVp',NULL,'192.168.1.106','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) Gecko/20100101 Firefox/83.0','YTozOntzOjY6Il90b2tlbiI7czo0MDoiOFZoUVFjRVdRVDNZM0w0aUo4bktGbFBNV1huWmJ2Q2w0QmUwR0U2MiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzE6Imh0dHA6Ly8xOTIuMTY4LjEuMTA2OjgwMDEvbG9naW4iO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19',1611109602),('iPfCIaprbDODxhJwnEclVhxsuWjZNl2ypR7mPVCI',NULL,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) Gecko/20100101 Firefox/83.0','YTozOntzOjY6Il90b2tlbiI7czo0MDoibWhyTERxWlY3WENRek9VcktRaXBwWklQa3NwWDFHcEJxekQ1WFVlUyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTQ6Imh0dHA6Ly8wLjAuMC4wIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==',1610893111),('myJtUlDkxCOoSNKqvwaHd6XA9yEzLcYGrX9wvOlj',NULL,'127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) Gecko/20100101 Firefox/83.0','YTozOntzOjY6Il90b2tlbiI7czo0MDoiVURDN1VEc2VHQlhQR2syS1FhR0NabHpvVGpONWl5UnRsWVdxRlVUUyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTQ6Imh0dHA6Ly8wLjAuMC4wIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==',1609854456),('uLjs7kITR6IfDR5L99yQYHZdHhDJY0Y8g3tLlDxb',1,'192.168.1.102','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) Gecko/20100101 Firefox/83.0','YTo2OntzOjY6Il90b2tlbiI7czo0MDoiYUJDR01yeGY5ZlBLVmZGRzBOc05WcWpjSjk5eDF6YnltWnM2WHNyNSI7czozOiJ1cmwiO2E6MDp7fXM6OToiX3ByZXZpb3VzIjthOjE6e3M6MzoidXJsIjtzOjQ0OiJodHRwOi8vMTkyLjE2OC4xLjEwMjo4MDAxL2FkbWluL3VzZXJzLzQvZWRpdCI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fXM6NTA6ImxvZ2luX3dlYl81OWJhMzZhZGRjMmIyZjk0MDE1ODBmMDE0YzdmNThlYTRlMzA5ODlkIjtpOjE7czoxNzoicGFzc3dvcmRfaGFzaF93ZWIiO3M6NjA6IiQyeSQxMCRWaHVTcmsuSUN2bi9lc0FpWVVIOEEuT2djU2JkUkJNanRYRG1wRzNkTi5qVS5wNWdmTDN4UyI7fQ==',1609394084),('ut7etlz3UOEtkxaAGIELQycovEuCrEIiot58cxu9',NULL,'192.168.1.102','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) Gecko/20100101 Firefox/83.0','YTozOntzOjY6Il90b2tlbiI7czo0MDoiWmlNTFJQOG91cjFQYTZjNUY5SlB3SHRkN0NvdXNzdGNGcUMwa3VPTCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzE6Imh0dHA6Ly8xOTIuMTY4LjEuMTAyOjgwMDEvbG9naW4iO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19',1610036477),('zUQoDc9koEBZ8kxffvSpW10XxCi3HtcYCXZDYoTO',NULL,'192.168.1.106','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0','YTozOntzOjY6Il90b2tlbiI7czo0MDoiMWhIN3lQZW41YUlqaEtzZVVsOUV4aEtxMnZZSEl3TEFHaHpoRnpIViI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjU6Imh0dHA6Ly8xOTIuMTY4LjEuMTA2OjgwMDEiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19',1611975987);
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_team_id` bigint(20) unsigned DEFAULT NULL,
  `profile_photo_path` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role` enum('ADMIN','MANAGER_PROJECT') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ADMIN',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Angel Pedro','arc.quars@gmail.com',NULL,'$2y$10$VhuSrk.ICvn/esAiYUH8A.OgcSbdRBMjtXDmpG3dN.jU.p5gfL3xS',NULL,NULL,NULL,NULL,NULL,'2020-10-10 17:57:12','2020-10-10 17:57:12','ADMIN'),(2,'Gonzalo Viraca','vidangos84@hotmail.com',NULL,'$2y$10$6Wr2YL.xWWODxx5h/7aLBOjF3x6nE5Gvz.782/QCUDsl0pF.N6Fri',NULL,NULL,NULL,NULL,NULL,'2020-10-10 17:59:23','2020-10-10 17:59:23','ADMIN'),(3,'Prueba','prueba@vrc.com',NULL,'$2y$10$bmgK/1Qlvdn.DK7UkupR9u.cW0XVW9ihoHkE72fzhmlp6rdJ.P/QW',NULL,NULL,NULL,NULL,NULL,'2020-10-10 19:44:18','2020-10-10 19:44:18','MANAGER_PROJECT'),(4,'Juan','arc12@lugu.net',NULL,'$2y$10$7pPAa8eqifbLyVPcwksa6uGG9tFpAwj32BrFQ/lyXVxl8ZX1pDlL6',NULL,NULL,NULL,NULL,NULL,'2020-12-31 09:47:25','2020-12-31 09:47:25','MANAGER_PROJECT'),(5,'Gui Juan','arc121@lugu.net',NULL,'$2y$10$HiIQL9EJMmzcrlo9RtFIDOUcuVoS2MepEAPmlaXUbyex78J7De9du',NULL,NULL,NULL,NULL,NULL,'2020-12-31 09:49:06','2020-12-31 09:49:06','MANAGER_PROJECT');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-01 21:00:12
